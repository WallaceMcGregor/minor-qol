"use strict";

import AbilityTemplate from "../../../systems/dnd5e/module/pixi/ability-template.js";
import AbilityUseDialog from "../../../systems/dnd5e/module/apps/ability-use-dialog.js";


let debug = false;
let debugDamageApply = false;
let log = (...args) => console.log("Minor Qol | ", ...args);

var knownSheets = undefined;

// modules that are known about
var dynamicEffectsActive;
var aboutTimeActive;
var betterRollsActive;
var lmrtfyActive;
var lmrtfySupportsAttach;
var diceSoNiceActive;

let cleanSpellName = (name) => {
  return name.toLowerCase().replace(/[^가-힣一-龠ぁ-ゔァ-ヴーa-zA-Z0-9ａ-ｚＡ-Ｚ０-９々〆〤]/g, '').replace("'", '').replace(/ /g, '');
}

// Strings to look for in chatmessages
var attackRollFlavor;
var attackRollFlavorAlt;
var damageRollFlavor;
var damageRollFlavorAlt;
var savingThrowText;
var savingThrowTextAlt;
var undoDamageText;
var traitList = {}; 

// Spells that do no damage when saved against.
let noDamageSaves = [];

const MESSAGETYPES = {
    hitData: 1,
    saveData: 2
};

const hiddenName = "-???-";


function resetStatus(versatile=false) {
  if (MinorQOL._stateData.targetHookId) Hooks.off("minor-qol-targeted", MinorQOL._stateData.targetHookId); // In case a roll was aborted don't continue the workflow
  MinorQOL._stateData.targetHookId = 0;
  MinorQOL._stateData.verstatile = versatile;
  MinorQOL._stateData.hitTargets = game.user.targets; 
  MinorQOL._stateData.isCritical = undefined; 
  MinorQOL._stateData.isFumble = undefined; 
  MinorQOL._stateData.saves = new Set(); 
  MinorQOL._stateData.failedSaves = new Set();
  MinorQOL._stateData.versatile = versatile;
  MinorQOL._stateData.targetDispositions = [-1, 0, 1];
  MinorQOL._stateData.speaker = ChatMessage.getSpeaker();
  MinorQOL._stateData.extraText = "";
  MinorQOL._stateData.hideSaves = false;
  MinorQOL._stateData.saveCount = 0;
  MinorQOL._stateData.item = null;
  MinorQOL._stateData.cardItem = null;
  MinorQOL._stateData.actor = null;
  MinorQOL._stateData.token = null;
  MinorQOL._stateData.spellLevel = undefined;
  MinorQOL._stateData.attackRoll = undefined;
  MinorQOL._stateData.attackTotal = undefined;
  MinorQOL._stateData.damageTotal = undefined;
  MinorQOL._stateData.damageRoll = undefined;
  MinorQOL._stateData.isHit = undefined;
  MinorQOL._stateData.item = undefined;
  MinorQOL._stateData.actor = undefined;
  MinorQOL._stateData.token = undefined;
  // Hooks.callAll("diceSoNiceRollComplete")
}
function notifyComplete() {
  Hooks.callAll("MinorQolRollComplete", {
    isCritical: MinorQOL._stateData.isCritical, 
    isFumble: MinorQOL._stateData.isFumble,
    isHit: MinorQOL._stateData.isHit,
    versatile: MinorQOL._stateData.verstatile,
    hitTargets: new Set(MinorQOL._stateData.hitTargets),
    saves: new Set(MinorQOL._stateData.saves),
    failedSaves: new Set (MinorQOL._stateData.failedSaves),
    targets: new Set(game.user.targets),
    spellLevel: MinorQOL._stateData.spellLevel,
    attackRoll: MinorQOL._stateData.attackRoll,
    attackTotal: MinorQOL._stateData.attackTotal,
    damageRoll: MinorQOL._stateData.damageRoll,
    damageTotal: MinorQOL._stateData.damageTotal,
    damageList: MinorQOL._stateData.damageList,
    item: MinorQOL._stateData.item,
    actor: MinorQOL._stateData.actor,
    token: MinorQOL._stateData.token
  })
}
let hotbarHandler = (bar, data, slot) => {
  if(debug) log("Hotbar Hook fired", bar, data, slot);
  if (!macroSpeedRolls) return true;
  if (data.type !== "Item") return true;
  if (debug) log("hotbar drop", "Creating macro");
  createMinorQOLMacro(data.data, slot);
  return false;
}

export function initSetup() {
  if (debug) log("Init Setup entered")
  fetchParams(true);
  // Watch for minor-qol hidden names and reveal them to the GM
  setupHiddenNameButtons();
    // when fixed replace with Hooks.on("hotbarDrop", hotbarHandler)
  Hooks._hooks.hotbarDrop = [hotbarHandler].concat(Hooks._hooks.hotbarDrop || []);
    // Deselection.init());
    ["di", "dv", "dr"].forEach(t=>traitList[t] = t);
}

export function readySetup() {
  if (debug) log("Ready Setup entered")

  // get all the game.settings.("minor-qol", ...)
  fetchParams();
  MinorQOL._stateData.hideSaves = false;

  if (!knownSheets) {
      console.error("Minor-qol | known sheets not setup - module not installed");
      return;
  }

  for (let sheetName of Object.keys(knownSheets)) {
      Hooks.on("render" + sheetName, enableSheetQOL);
  }
  Hooks.on("renderedAlt5eSheet", enableSheetQOL);
  Hooks.on("renderedTidy5eSheet", enableSheetQOL);

  // When a measured template is display auto target actors it auto target set
  Hooks.on("createMeasuredTemplate", selectTargets);
  // respond to chat rolls (attack and damage trigger further actions)
  setupRollHandling();

  // need to wait for translations to be loaded before setting these
  attackRollFlavor = i18n("minor-qol.attackRollFlavorText");
  damageRollFlavor = i18n("minor-qol.damageRollFlavorText");
  savingThrowText = i18n("minor-qol.savingThrowText");
  attackRollFlavorAlt = i18n("minor-qol.attackRollFlavorTextAlt");
  damageRollFlavorAlt = i18n("minor-qol.damageRollFlavorTextAlt");
  savingThrowTextAlt = i18n("minor-qol.savingThrowTextAlt");
  undoDamageText = i18n("minor-qol.undoDamageFrom");
  traitList.di = i18n("DND5E.DamImm");
  traitList.dr = i18n("DND5E.DamRes");
  traitList.dv = i18n("DND5E.DamVuln");
  traitList.dv = i18n("DND5E.DamVuln");

  var prevCombatant = "";
  // setup for removing targets at end of turn.
  Hooks.on("updateCombat", (combat, update, options, user) => {
    if (autoRemoveTargets !== "all") return;
    if (prevCombatant === "" && !game.user.isGM) { // have not seen any yet
      prevCombatant = combat.current.tokenId;
      return;
    } else {
      if (game.user.isGM || canvas.tokens.controlled.map(t=>t.id).includes(prevCombatant)) {
        removeAllTargets()
      }
      prevCombatant = combat.current.tokenId;
      return;
    }
  })

  setupSocket();
  noDamageSaves = i18n("minor-qol.noDamageonSaveSpells").map(name => cleanSpellName(name));
  console.log("minor-qol | No damage on save spells are ", noDamageSaves)
  Hooks.on("MinorQolPreItemRoll", checkRange);
  Hooks.on("MinorQolPreItemRoll", checkIncapcitated);
  // Check for other modules that we can use
  // Dynamiceffects active means we can apply active effects when an item is rolled.
  dynamicEffectsActive = game.modules.get("dynamiceffects")?.active;

  // Don't do anything based on this, but it is nice to know.
  aboutTimeActive = game.modules.get("about-time")?.active;

  //Better rolls active means we will check for better rolls chat cards.
  betterRollsActive = game.modules.get("betterrolls5e")?.active;
  checkBetterRolls = betterRollsActive;

  // LMRTFY active means we can have players roll their own saves via LMRTFY
  lmrtfyActive = game.modules.get("lmrtfy")?.active;
  if (!lmrtfyActive && ["letme", "letmeQuery"].includes(playerRollSaves)) {
    ui.notifications.warn("You have not installed LMRTFY but want to use it for player saves")
    console.error("LMRTFY not installed but use LMRTFY specified for player saves")
  }
  
  lmrtfySupportsAttach = lmrtfyActive && isNewerVersion(game.modules.get("lmrtfy").data.version, "0.9.0");
  // version after 0.9 supports tagging saving roll messages so we can distinguish which item the roll applies to.
  if (game.user.isGM && lmrtfyActive && !lmrtfySupportsAttach) 
    console.warn("LMRTFY version 0.9 and earlier does not support tagged saving throws please upgrade to a later version");

  // dice so nice 3d rolls needs to be 2.0.2 or later for us to be notified when the rolls are complete
  diceSoNiceActive = game.modules.get("dice-so-nice")?.active && isNewerVersion(game.modules.get("dice-so-nice").data.version, "2.0.2")

  // cozy-player resets targeting before we get to see the attack rolls which breaks auto targeting, damage etc
  if (game.modules.get("cozy-player")?.active && game.settings.get("cozy-player", "targetsClearOnRoll")) {
    ui.notifications.warn("Minor-qol auto effects (attack, damage, saving throws, damage application) disabled by cozyplayer Targets Clear on Attach", {permanent: true})
    console.warn("Minor-qol auto checks disabled by cozyplayer Targets Clear on Attach")
  }

  setTimeout( () => 
    Hooks.on("preCreateChatMessage", (data, options) => {
      if (useTokenNames && data.speaker?.token)  data.speaker.alias = canvas.tokens.get(data.speaker.token).name;
      return true;
    }), 1000);
}

let itemRollButtons, speedItemRolls, autoShiftClick, autoTarget;
let autoCheckHit, autoCheckSaves, autoRollDamage, criticalDamage;
let addChatDamageButtons, autoApplyDamage, damageImmunities;
let macroSpeedRolls, hideNPCNames, useTokenNames, itemDeleteCheck, nsaFlag;
let autoItemEffects, coloredBorders, rangeTarget, autoRemoveTargets;
let checkBetterRolls, playerRollSaves, playerSaveTimeout, preRollChecks;
let saveRequests = {};
let saveTimeouts = {};

export let fetchParams = (silent = false) => {
  itemRollButtons = game.settings.get("minor-qol", "ItemRollButtons");
  speedItemRolls = game.settings.get("minor-qol", "SpeedItemRolls");
  autoShiftClick = game.settings.get("minor-qol", "AutoShiftClick");
  autoTarget = game.settings.get("minor-qol", "AutoTarget");
  autoCheckHit = game.settings.get("minor-qol", "AutoCheckHit");
  autoRemoveTargets = game.settings.get("minor-qol", "AutoRemoveTargets");
  autoCheckSaves = game.settings.get("minor-qol", "AutoCheckSaves");
  autoRollDamage = game.settings.get("minor-qol", "AutoRollDamage");
  criticalDamage = game.settings.get("minor-qol", "CriticalDamage");
  addChatDamageButtons = game.settings.get("minor-qol", "AddChatDamageButtons");
  autoApplyDamage = game.settings.get("minor-qol", "AutoApplyDamage");
  damageImmunities = game.settings.get("minor-qol", "DamageImmunities");
  macroSpeedRolls = game.settings.get("minor-qol", "MacroSpeedRolls");
  hideNPCNames = game.settings.get("minor-qol", "HideNPCNames");
  useTokenNames = game.settings.get("minor-qol", "UseTokenNames")
  itemDeleteCheck = game.settings.get("minor-qol", "ItemDeleteCheck");
  nsaFlag = game.settings.get("minor-qol", "showGM");
  autoItemEffects = game.settings.get("minor-qol", "AutoEffects");
  coloredBorders = game.settings.get("minor-qol", "ColoredBorders");
  rangeTarget = game.settings.get("minor-qol", "RangeTarget");
  playerRollSaves = game.settings.get("minor-qol", "PlayerRollSaves")
  playerSaveTimeout = game.settings.get("minor-qol", "PlayerSaveTimeout")
  preRollChecks = game.settings.get("minor-qol", "PreRollChecks")
  if (!silent) {
    if (debugDamageApply) {
      console.log("minor-qol | Fetch Params called ", getParams(), silent); 
      ChatMessage.create( {
        flavor: `Fetch Params called ${game.user.id}`,
        whisper: ChatMessage.getWhisperRecipients("GM").filter(u=>u.active),
        content: getParams()
      }) 
    }
  }
}

let getParams = () => {
  return ` 
    itemRollButtons: ${itemRollButtons} <br>
    speedItemRolls: ${speedItemRolls} <br>
    autoTarget: ${autoTarget} <br>
    autoCheckHit: ${autoCheckHit} <br>
    autoCheckSaves: ${autoCheckSaves} <br>
    autoApplyDamage: ${autoApplyDamage} <br>
    autoRollDamage: ${autoRollDamage} <br>
    playerRollSaves: ${playerRollSaves} <br>
    checkBetterRolls: ${checkBetterRolls} `
}
let processSecretMessage = (data, options) => {
  if (debug) log("minor-qol | processing secret message")
  if (!data.whisper  || data.whisper.length === 0) return true;
  let gmIds = ChatMessage.getWhisperRecipients("GM").filter(u=>u.active).map(u=>u.id);
  let currentIds = data.whisper.map(u=>typeof(u) === "string" ? u : u.id);
  gmIds = gmIds.filter(id => !currentIds.includes(id));
  data.whisper = data.whisper.concat(gmIds);
  return true;
}

/**
 * If we are auto checking saves disable the display of created saves - checkSaves will display a combo card instead
 * @param {[message]} messages 
 * @param {} data 
 * @param {} options 
 */
let processpreCreateSaveRoll = (data, options) => {
  if (!MinorQOL._stateData.hideSaves || !autoCheckSaves) {
    MinorQOL._stateData.saveCount = 0;
    MinorQOL._stateData.hideSaves = false;
    return true;
  }
  if (data.user !== game.user.id) return true;
  options.displaySheet = false;
  MinorQOL._stateData.saveCount -= 1;
  MinorQOL._stateData.hideSaves = MinorQOL._stateData.saveCount > 0;
  return false;
}

let processPreCreateDamageRoll = (data, options) => {
  if (criticalDamage === "default") return;

  if (!isNewerVersion(game.data.version, "0.6.9")) return;

  if (data.flavor.includes(i18n("minor-qol.criticalText") || data.flavor.includes(i18n("minor-qol.criticalTextAlt")))) {
    let r = Roll.fromJSON(data.roll);
    let rollBase = new Roll(r.formula);
    if (criticalDamage === "maxDamage") {
      rollBase.terms = rollBase.terms.map(t => {
        if (t?.number) t.number = t.number/2;
        return t;
      });
      rollBase.evaluate({maximize: true});
      rollBase._formula = rollBase.formula;
      data.roll = JSON.stringify(rollBase);
      data.content = `${rollBase.total}`;
    } else if (criticalDamage === "maxCrit") {
      let rollCrit = new Roll(r.formula);
      rollCrit.terms = rollCrit.terms.map(t => {
        if (t?.number) t.number = t.number/2;
        if (typeof t === "number") t = 0;
        return t;
      });
      rollBase.terms = rollBase.terms.map(t => {
        if (t?.number) t.number = t.number/2;
        return t;
      });
      rollCrit.evaluate({maximize: true});
      rollBase.terms.push("+")
      rollBase.terms.push(rollCrit.total)
      rollBase._formula = rollBase.formula;
      rollBase.roll();
      data.total = rollBase.total;
      data.roll = JSON.stringify(rollBase);
    } else if (criticalDamage === "maxAll") {
      rollBase.evaluate({maximize: true});
      data.roll = JSON.stringify(rollBase);
      data.content = `${rollBase.total}`;
    }
  }
  return true;
}

let processBetterRollsChatCard = (message, html, data) => {
  if (debug) log("processBetterRollsChatCard", message. html, data)
  const requestId = message.data.speaker.actor;
  if (!saveRequests[requestId]) return true;
  const title = html.find(".item-name")[0]?.innerHTML
  if (!title) return true;
  if (!title.includes("Save")) return true;
  const formula = "1d20";
  const total = html.find(".dice-total")[0]?.innerHTML;
  clearTimeout(saveTimeouts[requestId]);
  saveRequests[requestId]({total, formula})
  delete saveRequests[requestId];
  delete saveTimeouts[requestId];
  return true;
}

let processpreCreateBetterRollsCard = async (data, options) => {
  const requestId = data.speaker.token;
  let html = $(data.content);
  const title = html.find(".item-name")[0]?.innerHTML;
  resetStatus();
  let rollDivs = html.find(".dice-roll.red-dual");//.find(".dice-row-item");
  let rollData = html.find("red-full");
  if (debug) log("better rolls ", rollData, rollDivs)

  let itemId = html[0].attributes["data-item-id"];
  if (!itemId) return true; // not an item roll.

  itemId = itemId.nodeValue;
  let itemRe = /[^(]\(([\d]*)[^)]*\)/
  let actor = game.actors.tokens[data.speaker.token];
  if (!actor) actor = game.actors.get(data.speaker.actor);
  let item = actor.items.get(itemId);

  let levelMatch =  title.match(itemRe);
  let itemLevel = levelMatch ? levelMatch[1] : (item.data.data.level || 0);
  MinorQOL._stateData.spellLevel = itemLevel;
  let damageStart = 0;
  let diceRoll;

  if (item.hasAttack) {
    damageStart = 1
    const attackRolls = $(rollDivs[0]).find(".dice-total");
    let diceRolls = $(rollDivs[0]).find(".roll.die.d20");
    for (let i = 0; i < attackRolls.length; i++) {
      if (!attackRolls[i].classList.value.includes("ignore")) {
        MinorQOL._stateData.attackTotal = parseInt(attackRolls[i]?.innerHTML);
        diceRoll = parseInt(diceRolls[i]?.innerHTML);
        break;
      }
    }
  }

  // each weapon has it's own critical threshold
  let criticalThreshold = item.data.flags.betterRolls5e?.critRange?.value || 20;
  if (item.data.type === "weapon") criticalThreshold = Math.min(criticalThreshold, actor.data.flags.dnd5e?.weaponCriticalThreshold || 20);
  MinorQOL._stateData.isCritical = diceRoll >= criticalThreshold;

  MinorQOL._stateData.damageList = [];
  if (debug) log("Better Rolls Chat card", title, itemLevel, MinorQOL._stateData.attackTotal, damageStart, MinorQOL._stateData.isCritical)


  // We can't process the targets immediately since we might be still placing the spell template
  MinorQOL._stateData.targetHookId = Hooks.once("minor-qol-targeted", async () => {
    if (debug) log("Better rolls minor-qol-targeted ", MinorQOL._stateData)
    document.activeElement.blur();
    for (let i = damageStart; i < rollDivs.length; i++) {
      let child = rollDivs[i].children;
      let damage = 0;
      // Structure is [flavor-text, dice-result]. If there is no flavor-text use the first else the second
      let resultIndex = child.length === 1 ? 0 : 1;
      for (let j = 0; j < $(child[resultIndex]).find(".dice-total")[0]?.children?.length; j++) {
        let damageDiv = $(child[resultIndex]).find(".dice-total")[0].children[j];
        // see if this damage is critical damage or not
        let isCriticalDamage = false;
        if (!MinorQOL._stateData.isCritical) {
          for (let k = 0; k < damageDiv.classList.length; k++) {
            if (damageDiv.classList[k] === "red-crit-damage" ) isCriticalDamage = true;
          }
        }
        if (!MinorQOL._stateData.isCritical && isCriticalDamage) continue;
        let damageitem = parseInt(damageDiv.innerHTML);
        if (!isNaN(damageitem)) damage += damageitem;
      }
      const typeString = child[0].innerHTML;
      let type = (Object.entries(CONFIG.DND5E.damageTypes).find(entry => typeString.includes(entry[1])) || ["unmatched"])[0];
      if (type === "unmatched") type = (Object.entries(CONFIG.DND5E.healingTypes).find(entry => typeString.includes(entry[1])) || ["unmatched"])[0];
      MinorQOL._stateData.damageList.push({type, damage})
    }
    MinorQOL._stateData.theTargets = (item?.data.data.target?.type === "self") ? getSelfTargetSet(actor) : new Set(game.user.targets);
    MinorQOL._stateData.hitTargets = new Set(MinorQOL._stateData.theTargets)
    MinorQOL._stateData.isFumble = diceRoll === 1;
    MinorQOL._stateData.isHit = true;
    if (autoCheckHit !== "none" && item.hasAttack && MinorQOL._stateData.attackTotal) {
      // let theTargets = game.user.targets;
      let msg = "";
      let sep = "";
      
      // check for a hit/critical/fumble
      MinorQOL._stateData.isHit = false;
      MinorQOL._stateData.hitTargets = new Set();
      let hitDisplay = [];

      const attackType = item?.name ? i18n(item.name) : "Attack";
      for (let targetToken of MinorQOL._stateData.theTargets) {
        let targetActor = targetToken.actor;
        let targetAC = targetActor.data.data.attributes.ac.value;
        if (!MinorQOL._stateData.isFumble && !MinorQOL._stateData.isCritical) {
            // check to see if the roll hit the target
            if (game.user.isGM) log(`${data.speaker.alias} Rolled a ${MinorQOL._stateData.attackTotal} to hit ${targetActor.name}s AC of ${targetAC}`);
            MinorQOL._stateData.isHit = MinorQOL._stateData.attackTotal >= targetAC;
        }
        MinorQOL._stateData.extraText = `${data.speaker.alias} Rolled a ${MinorQOL._stateData.attackTotal} to hit ${targetActor.name}s AC of ${targetAC}`

        // Log the hit on the target
        let hitString = MinorQOL._stateData.isCritical ? i18n("minor-qol.criticals") : MinorQOL._stateData.isFumble? i18n("minor-qol.fumbles") : MinorQOL._stateData.isHit ? i18n("minor-qol.hits") : i18n("minor-qol.misses");
        let img = targetToken.data.img || targetToken.actor.img;
        if ( VideoHelper.hasVideoExtension(img) ) {
          img = await game.video.createThumbnail(img, {width: 100, height: 100});
        }
        hitDisplay.push({isPC: targetToken.actor.isPC, target: targetToken, hitString, attackType, img});
        // If we hit and we have targets and we are applying damage say so.
        if (MinorQOL._stateData.isHit || MinorQOL._stateData.isCritical) MinorQOL._stateData.hitTargets.add(targetToken);
      }
      let damageNotApplied = (autoApplyDamage === "none") 
                            || MinorQOL._stateData.hitTargets.size === 0 // no targets
                            || (!MinorQOL.forceRollDamage && autoRollDamage !== "none")  // we do not roll damage
                            || (item?.hasSave && autoCheckSaves === "none") // item has a save but we are not rolling saves
      let templateData = {
        attackType: attackType,
        hits: hitDisplay, 
        isGM: game.user.isGM,
        damageAppliedString: damageNotApplied ? "" : i18n("minor-qol.damage-applied")
      }
      let content = await renderTemplate("modules/minor-qol/templates/hits.html", templateData);
      let speaker = data.speaker;
      speaker.alias = (useTokenNames && speaker.token) ? canvas.tokens.get(speaker.token).name : speaker.alias;
      if (MinorQOL._stateData.hitTargets.size > 0) {
        let chatData = {
          user: ChatMessage.getWhisperRecipients("GM").find(u=>u.active),
          speaker,
          content: content,
          type: CONST.CHAT_MESSAGE_TYPES.OTHER,
          flags: { minorQolType: MESSAGETYPES.hitData }
        }
        if (autoCheckHit === "whisper" || ["gmroll", "blindroll"].includes(data.rollMode)) {
          chatData.whisper = ChatMessage.getWhisperRecipients("GM").filter(u=>u.active);
          chatData.user = ChatMessage.getWhisperRecipients("GM").find(u=>u.active);
        }
        ChatMessage.create(chatData);
      }
    }
    checkSaves(MinorQOL._stateData.hitTargets, item, ["gmroll", "blindroll"].includes(data.rollMode)).then((saves) => {
      MinorQOL._stateData.damageTotal = MinorQOL._stateData.damageList.reduce((acc, a) => a.damage + acc, 0);
      let appliedDamage = 0;
      if (autoApplyDamage !== "none" && !(item.hasSave && autoCheckSaves === "none")) {
        //let theTargets = MinorQOL._stateData.hitTargets;
        //let saves = MinorQOL._stateData.saves;
        if(autoCheckSaves === "none") saves = MinorQOL._stateData.saves;
        if (MinorQOL._stateData.damageTotal > 0) appliedDamage = applyTokenDamage(MinorQOL._stateData.damageList, MinorQOL._stateData.damageTotal, MinorQOL._stateData.hitTargets, item, saves);
      }
  
      if (item && autoItemEffects && dynamicEffectsActive && data.user ===  game.user._id) { // perhaps apply item effects
        // if someone saved we want the failedSaves to apply effects if no-one saved then all targets get the effect.
        let theTargets = MinorQOL._stateData.theTargets;
        if (item.hasAttack) theTargets = MinorQOL._stateData.hitTargets;
        if (autoCheckSaves !== "none" && item.hasSave) { // if we did no checking of saves, don't update the data
          theTargets = MinorQOL._stateData.failedSaves;
        }
        if ( (autoCheckHit !== "none" && item.hasAttack)
            || (autoCheckSaves && item.hasSave)
            || (!item.hasAttack && !item.hasSave)) {
        // assume effects only applied to hit targets or thtose that did not save
          let spellLevel = MinorQOL._stateData.spellLevel;
          if (theTargets?.size > 0 || (!item.hasAttack && !item.hasSave))
            DynamicEffects.doEffects({item, actor: item.actor, activate: true, targets: theTargets, 
                                  whisper: true, spellLevel: itemLevel || 0, damageTotal: theTargets.size > 1 ? MinorQOL._stateData.damageTotal : appliedDamage}) 
        }
      }
      notifyComplete()
      resetStatus();
      filterTargets();
    })
  })

  // If we are not auto targetting then cause the rest of the processing to fire.
  if (!autoTarget || !item.hasAreaTarget) Hooks.callAll("minor-qol-targeted");
}

/**
 * Set handlers for attack and damage rolls
 */
let setupRollHandling = () => {
  Hooks.on("preCreateChatMessage", (data, options, other) => {
    if (debug) log("preCreateChatMessage hook", data, options);
    if (nsaFlag) processSecretMessage(data,options);
    if (checkBetterRolls && data.content?.startsWith('<div class="dnd5e red-full chat-card"'))
    {
      return processpreCreateBetterRollsCard(data, options)
    }

    if (!data?.flavor) return true;
    // if (data.content.includes(`div class="beyond20-message">`)) return procsesCreatBeyond20Message(data. options);
    if (data.flavor.includes(attackRollFlavor)) return processPreCreateAttackRoll(data, options);
    if (data.flavor.includes(damageRollFlavor)) return processPreCreateDamageRoll(data, options);
    if (data.flavor.includes(savingThrowText)) return processpreCreateSaveRoll(data, options);
    if (attackRollFlavorAlt?.length > 0 && data.flavor.includes(attackRollFlavorAlt)) return processPreCreateAttackRoll(data, options);
    if (damageRollFlavorAlt?.length > 0 && data.flavor.includes(damageRollFlavorAlt)) return processPreCreateDamageRoll(data, options);
    if (savingThrowTextAlt?.length > 0 && data.flavor.includes(savingThrowTextAlt)) return processpreCreateSaveRoll(data, options);
    return true;
  });

  Hooks.on("renderChatMessage", (message, html, data) => {
    if (checkBetterRolls && message?.data?.content?.startsWith('<div class="dnd5e red-full chat-card"')) return processBetterRollsChatCard(message, html, data)
    if (debug) log("renderChatMessage hook", message, html, data, attackRollFlavor, damageRollFlavor);
      if (!message.data?.flavor) return true;

      let flavor = message.data.flavor;
      if (flavor.includes(i18n("minor-qol.saving-throw"))) return processMQSaveRoll(message, html, data);
      if (flavor.startsWith(undoDamageText)) return processUndoDamageCard(message, html, data);
      if (!message.isRoll) return true;
      if (flavor.includes(attackRollFlavor)) return processAttackRoll(message, html, data);
      if (flavor.includes(damageRollFlavor)) return processDamageRoll(message, html, data);
      if (flavor.includes(savingThrowText)) return processSaveRoll(message, html, data);
      if (attackRollFlavorAlt?.length > 0 && flavor.includes(attackRollFlavorAlt)) return processAttackRoll(message, html, data);
      if (damageRollFlavorAlt?.length > 0 && flavor.includes(damageRollFlavorAlt)) return processDamageRoll(message, html, data);
      if (savingThrowTextAlt?.length > 0 && flavor.includes(savingThrowTextAlt)) return processSaveRoll(message, html, data);
      return true;
  });
};



let enableSheetQOL = (app, html, data) => {
    const tabs = []
    
    html.find('a[data-tab]').each(
      function() {
        const tabName = $(this).data('tab')
        tabs.push(`.${tabName}`);
      }
    )

    // find out how to reinstate the original handler later.
    const defaultTag = ".item .item-image";
    //Add a check for item deletion
    if (itemDeleteCheck) {
        // remove current handler - this is a bit clunky since it results in a case with no delete handler
        $(html).find(".item-delete").off("click");
        $(html).find(".item-delete").click({ app, data: data }, itemDeleteHandler);
    }

    let rollTag = knownSheets[app.constructor.name] ? knownSheets[app.constructor.name] : defaultTag;

    if (itemRollButtons)
        addItemSheetButtons(app, html, data);

    if (speedItemRolls !== "off") {
      tabs.forEach(tab => {
        // Item Rolling do attack and damge at the same
        if (["BetterNPCActor5eSheet", "BetterNPCActor5eSheetDark", "MonsterBlock5e"].includes(app.constructor.name)) 
          var rollTagElement = html.find(`${rollTag}`); // do not have the right nav tabs
        else
          var rollTagElement = html.find(`${tab} ${rollTag}`);
        rollTagElement.off("click");
        rollTagElement.off("click", "minor-qol", itemRollHandler);
        rollTagElement.on("click", {app, data, html}, itemRollHandler);
        rollTagElement.off("contextmenu", "minor-qol", itemRollHandler);
        rollTagElement.off("contextmenu");
        rollTagElement.on("contextmenu", { app, data, html }, itemRollHandler);
      })
    }
    return true;
};


function addItemSheetButtons(app, html, data, triggeringElement = "", buttonContainer = "") {
    // Setting default element selectors
    if (triggeringElement === "")
        triggeringElement = ".item .item-name";

        if (["BetterNPCActor5eSheet", "BetterNPCActor5eSheetDark"].includes(app.constructor.name)) {
      triggeringElement = ".item .npc-item-name"
      buttonContainer = ".item-properties"
    }
    if (buttonContainer === "")
        buttonContainer = ".item-properties";

    // adding an event for when the description is shown
    html.find(triggeringElement).click(event => {
        let li = $(event.currentTarget).parents(".item");
        if (!li.hasClass("expanded")) return; 
        let item = app.object.getOwnedItem(li.attr("data-item-id"));
        if (!item) return;
        let actor = app.object;
        let chatData = item.getChatData();
        let targetHTML = $(event.target.parentNode.parentNode);
        let buttonTarget = targetHTML.find(".item-buttons");
        if (buttonTarget.length > 0) return; // already added buttons
        let buttonsWereAdded = false;
        // Create the buttons
        let buttons = $(`<div class="item-buttons"></div>`);
        switch (item.data.type) {
            case "weapon":
            case "spell":
            case "feat":
                if (speedItemRolls !== "off")
                    buttons.append(`<span class="tag"><button data-action="basicRoll">${i18n("minor-qol.buttons.roll")}</button></span>`);
                if (item.hasAttack)
                    buttons.append(`<span class="tag"><button data-action="attack">${i18n("minor-qol.buttons.attack")}</button></span>`);
                if (item.hasDamage)
                    buttons.append(`<span class="tag"><button data-action="damage">${i18n("minor-qol.buttons.damage")}</button></span>`);
                if (item.isVersatile) 
                    buttons.append(`<span class="tag"><button data-action="versatileAttack">${i18n("minor-qol.buttons.versatileAttack")}</button></span>`);
                if (item.isVersatile) 
                  buttons.append(`<span class="tag"><button data-action="versatileDamage">${i18n("minor-qol.buttons.versatileDamage")}</button></span>`);
                buttonsWereAdded = true;
                break;
            case "consumable":
                if (chatData.hasCharges)
                    buttons.append(`<span class="tag"><button data-action="consume">${i18n("minor-qol.buttons.itemUse")} ${item.name}</button></span>`);
                buttonsWereAdded = true;
                break;
            case "tool":
                buttons.append(`<span class="tag"><button data-action="toolCheck" data-ability="${chatData.ability.value}">${i18n("minor-qol.buttons.itemUse")} ${item.name}</button></span>`);
                buttonsWereAdded = true;
                break;
        }
        if (buttonsWereAdded) {
            buttons.append(`<br><header style="margin-top:6px"></header>`);
            // adding the buttons to the sheet

            targetHTML.find(buttonContainer).prepend(buttons);
            buttons.find("button").click({app, data, html}, async (ev) =>  {
                ev.preventDefault();
                ev.stopPropagation();
                if (debug) log("roll handler ", ev.target.dataset.action)
                // If speed rolls are off
                switch (ev.target.dataset.action) {
                    case "attack":
                        resetStatus();
                        await item.rollAttack({ event: ev });
                        break;
                    case "versatileAttack":
                        resetStatus();
                        MinorQOL._stateData.versatile = true;
                        await item.rollAttack({ event: ev });
                        break;
                    case "damage":
                        await item.rollDamage({ event: ev, versatile: false });
                        break;
                    case "versatileDamage":
                        await item.rollDamage({ event: ev, versatile: true });
                        break;
                    case "consume":
                        await item.roll({ event: ev });
                        break;
                    case "toolCheck":
                        await item.rollToolCheck({ event: ev });
                        break;
                    case "basicRoll":
                        if (item.type === "spell") {
                          MinorQOL._stateData.hitTargets = game.user.targets;
                          await actor.useSpell(item, { configureDialog: true });
                        }
                        else
                            await item.roll();
                        break;
                }
            });
        }
    });
}

async function itemRollHandler(event) {
  // Allow shift/ctl/alt from the weapon img - unshifted works as before
  //let actor = game.actors.get(event.data.data.actor._id);
  let actor;
  // If the app has a token then this is a token sheet and we want the actor inside the token
  if (event.data.app.token)
      actor = event.data.app.token.actor;
  else if (event.data.app.object)
      actor = event.data.app.object;
  // this should be defined
  else
      actor = game.actors.get(event.data.data.actor._id); // but just in case we can get the global Actor if we must

  let itemId = $(event.currentTarget).parents(".item").attr("data-item-id");
  if (!itemId) itemId = $(event.currentTarget).attr("data-item-id");
  if (!itemId) {
    console.error("Could not find item in character sheet")
    return false;
  }

  let item = actor.getOwnedItem(itemId);

  if (!item) {
    const dataset = $(event.currentTarget).parents(".item")[0].dataset;
    const mitemId = dataset.itemId;
    const magicItemId = dataset.magicItemId;
    const magicItemActor = MagicItems.actor(actor.id);
    magicItemActor.roll(magicItemId, mitemId);
    // see if we can get a magic item
    return true;
  }; // We can't process this so let the default run
  if (event.__proto__.stopPropagation) {
    event.preventDefault();
    event.stopImmediatePropagation()
    event.stopPropagation();
  }

  if (debug) log("itemRollhandler", itemId, item, actor)
  // Check pre roll checks
  if (preRollChecks && Hooks.call("MinorQolPreItemRoll", actor, item, event) === false) {
    console.warn("Minor-qol | item roll blocked by pre checks");
    return false;
  }
  if (speedItemRolls !== "off") {
    if (item.hasAttck || item.hasSave || item.hasDamage || autoItemEffects) {
      return await doCombinedRoll({actor, item, event});
    }
    if (item.type === "spell")  {
      return await actor.useSpell(item, { configureDialog: !event.shiftKey });
    }
    if (item.type === "tool") {
      setShiftOnly(event);
      return await item.rollToolCheck({ event });
    }
    if (item.type === "consumable") {
      setShiftOnly(event);
      return await item.roll({event});
      // return rollConsumable({item, event});
    }
  }
  return await item.roll({event});
}

// Fires on renderMeasuredTemplate.
// set game user targets
let selectTargets = (scene, data, options) => {
    let targeting = autoTarget;
    if (data.user !== game.user._id) {
        return true;
    }
    if (targeting === "none") {
      Hooks.callAll("minor-qol-targeted", game.user.targets);
      return true;
    } 
    if (data) {
      // release current targets
      game.user.targets.forEach(t => {
        t.setTarget(false, { releaseOthers: false });
      });
      game.user.targets.clear();
    }

    let speaker = MinorQOL._stateData.speaker;
    let item = MinorQOL._stateData.item;
    // if the item specifies a range of "self" don't target the caster.
    let selfTarget = !(item?.data.data.range?.units === "self")

    let wallsBlockTargeting = targeting === "wallsBlock";
    let templateDetails = canvas.templates.get(data._id);

    let tdx = data.x;
    let tdy = data.y;
 // Extract and prepare data
    let {direction, distance, angle, width} = data;
    distance *= canvas.scene.data.grid / canvas.scene.data.gridDistance;
    width *= canvas.scene.data.grid / canvas.scene.data.gridDistance;
    direction = toRadians(direction);

    var shape
  // Get the Template shape
  switch ( data.t ) {
    case "circle":
      shape = templateDetails._getCircleShape(distance);
      break;
    case "cone":
      shape = templateDetails._getConeShape(direction, angle, distance);
      break;
    case "rect":
      shape = templateDetails._getRectShape(direction, distance);
      break;
    case "ray":
      shape = templateDetails._getRayShape(direction, distance, width);
    }
    canvas.tokens.placeables.filter(t => {
      if (!t.actor) return false;
      // skip the caster
      if (!selfTarget && speaker.token === t.id) return false;
      // skip special tokens with a race of trigger
      if (t.actor.data.data.details.race === "trigger") return false;
      if (!shape.contains(t.center.x - tdx, t.center.y - tdy))
        return false;
      if (!wallsBlockTargeting)
        return true;
      // construct a ray and check for collision
      let r = new Ray({ x: t.center.x, y: t.center.y}, templateDetails.data);
      return !canvas.walls.checkCollision(r);
    }).forEach(t => {
      t.setTarget(true, { user: game.user, releaseOthers: false });
      game.user.targets.add(t);
    });
    game.user.broadcastActivity({targets: game.user.targets.ids});

    // Assumes area affect do not have a to hit roll
    MinorQOL._stateData.saves = new Set();
    MinorQOL._stateData.hitTargets = game.user.targets;
    Hooks.callAll("minor-qol-targeted", game.user.targets);
    return true;
};


let hasChargesAvailable = (item) => {
  const usesRecharge = !!item.data.data.recharge?.value;
  const uses = item.data.data.uses;
  let usesCharges = !!uses?.per && (uses.max > 0);
  if (!usesRecharge && !usesCharges) return true;
  if (usesRecharge && item.data.data.recharge.charged) return true;
  if (item.data.data.uses.value > 0) return true;
  ui.notifications.warn(`${game.i18n.localize("minor-qol.noCharges")}`)
 return false;
}
let consumeCharge = async (item) => {
    // Determine whether to deduct uses of the item
    
    let itemData = item.data.data;
    const uses = itemData.uses || {};
    let usesCharges = !!uses.per && (uses.max > 0);
    const recharge = itemData.recharge || {};
    const usesRecharge = !!recharge.value;
    if (!usesRecharge && !usesCharges) return true;
    // If no usages remain, display a warning
    const current = uses.value || 0;
    if ( current <= 0 ) {
      ui.notifications.warn(game.i18n.format("DND5E.ItemNoUses", {name: item.name}));
      return;
    }
    const remaining = usesCharges ? Math.max(current - 1, 0) : current;
    if ( usesRecharge ) await item.update({"data.recharge.charged": false});
    else {
      await item.update({"data.uses.value": remaining});
    }
}

let consumeConsumable = async (item) => {
    const itemData = item.data.data;

    // Determine whether to deduct uses of the item
    const uses = itemData.uses || {};
    const autoDestroy = uses.autoDestroy;
    let usesCharges = !!uses.per && (uses.max > 0);
    const recharge = itemData.recharge || {};
    const usesRecharge = !!recharge.value;

    const current = uses.value || 0;
    const remaining = usesCharges ? Math.max(current - 1, 0) : current;
    if ( usesRecharge ) {
      if (!recharge.charged) {
        ui.notifications.warn(game.i18n.format("DND5E.ItemNoUses", {name: item.name}));
        return false;
      }
      await item.update({"data.recharge.charged": false});
    }
    else {
      const q = itemData.quantity;
      // Case 1, reduce charges
      if ( remaining ) {
        return await item.update({"data.uses.value": remaining});
      }
      // Case 2, reduce quantity
      else if ( q > 1 ) {
        return await item.update({"data.quantity": q - 1, "data.uses.value": uses.max || 0});
      }
      // Case 3, destroy the item
      else if ( (q <= 1) && autoDestroy ) {
        return await item.actor.deleteOwnedItem(item.id);
      }
      // Case 4, reduce item quantity and 0 charges
      else if ( (q === 1) ) {
        return await item.update({"data.quantity": q - 1, "data.uses.value": 0});
      }
      // Case 5, item unusable, display warning and do nothing
      else {
        ui.notifications.warn(game.i18n.format("DND5E.ItemNoUses", {name: item.name}));
        return false;
      }
    }
  }


let doCombinedRoll = async ({actor, item, event, versatile=false, token = null}) => {
  // stage 1 - do spell casting requirements.
  if (debug) log("docombinedRoll", actor, item, event);
  if (event?.type === "contextmenu") versatile = true;
  resetStatus(versatile);

  if (token) MinorQOL._stateData.token = token;
  if (event?.data?.app?.token) MinorQOL._stateData.token = event.data.app.token;
  MinorQOL._stateData.item = item;
  MinorQOL._stateData.actor = actor;
  if (["feat", "tool", "weapon"].includes(item.type)) {
    if (!hasChargesAvailable(item)) return false;
  }
  MinorQOL._stateData.cardItem = item;

  // If this is a spell, 
  // 1. Make sure there are spell slots to cast and consume spell slot.
  // record the spell level so the damage roll will work.
  // 2. If it is a template roll, draw the template and use that to tirgger setting targets
  // 3. Cause finish roll to be called either directly or after the template placement

  let placeTemplate = true;
  let lvl;
  if (item.type === "spell") {
    const itemData = item.data.data;

    // Configure spellcasting data
    let lvl = itemData.level;
    MinorQOL._stateData.spellLevel = lvl;
    const usesSlots = (lvl > 0) && CONFIG.DND5E.spellUpcastModes.includes(itemData.preparation.mode);
    const limitedUses = !!itemData.uses.per;
    let consumeSlot = `spell${lvl}`;
    let consumeUse = false;
    let placeTemplate = false;

    // Configure spell slot consumption and measured template placement from the form
    if ( usesSlots || item.hasAreaTarget || limitedUses ) {
      const usage = await AbilityUseDialog.create(item);
      if ( usage === null ) return;

      // Determine consumption preferences
      consumeSlot = Boolean(usage.get("consumeSlot"));
      consumeUse = Boolean(usage.get("consumeUse"));
      placeTemplate = Boolean(usage.get("placeTemplate"));

      // Determine the cast spell level
      const isPact = usage.get('level') === 'pact';
      const lvl = isPact ? actor.data.data.spells.pact.level : parseInt(usage.get("level"));
      if ( lvl !== item.data.data.level ) {
        const upcastData = mergeObject(item.data, {"data.level": lvl}, {inplace: false});
        item = item.constructor.createOwned(upcastData, actor);
        MinorQOL._stateData.spellLevel = lvl;
      }

      // Denote the spell slot being consumed
      if ( consumeSlot ) consumeSlot = isPact ? "pact" : `spell${lvl}`;
    }

    // Update Actor data
    if ( usesSlots && consumeSlot && (lvl > 0) ) {
      const slots = parseInt(actor.data.data.spells[consumeSlot].value);
      if ( slots === 0 || Number.isNaN(slots) ) {
        return ui.notifications.error(game.i18n.localize("DND5E.SpellCastNoSlots"));
      }
      await actor.update({
        [`data.spells.${consumeSlot}.value`]: Math.max(parseInt(actor.data.data.spells[consumeSlot].value) - 1, 0)
      });
    }

    // Update Item data
    if ( limitedUses && consumeUse ) {
      const uses = parseInt(itemData.uses.value || 0);
      if ( uses <= 0 ) ui.notifications.warn(game.i18n.format("DND5E.ItemNoUses", {name: item.name}));
      await item.update({"data.uses.value": Math.max(parseInt(item.data.data.uses.value || 0) - 1, 0)})
    }
  }
  item = MinorQOL._stateData.item;

  // we are taking over the roll (If we got here speed item rolls is on or a macro called us).
  // if there is an attack (always roll attacks)
  // there is damage and we are auto rolling damage
  // there is no damage but a save and we are autochecking saves
  const canAffectTargets = item.hasAttack || (item.hasDamage && (MinorQOL.forceRollDamage || autoRollDamage !== "none")) || (item.hasSave && autoCheckSaves !== "none"); // query active effects?
  const willUseStandardRoll = !item.hasAttack && !item.hasDamage && !item.hasSave && item.type !== "tool";
  // we need to consume resources if we bypass item.roll()
  if (!willUseStandardRoll) { // || ["tool", "spell", "consumable"].includes(item.type)) {
    if (debug) log("doCombined Roll checking resources ", canAffectTargets, item.type)
    if (!await item._handleResourceConsumption({isCard: true, isAttack: false})) return;
    if (["feat", "tool"].includes(item.type)) {
      // These need to check charges as well as resources
      if (!hasChargesAvailable(item)) return false;
      await consumeCharge(item);
    }
    if(item.type === "consumable") {
      const result = await consumeConsumable(item);
      if (!result) return;
    }
  }


  // anything with an area of effect that might affect creatures
  if (item.hasAreaTarget && placeTemplate) {
    
    const template = AbilityTemplate.fromItem(item);
    // drawing the template removes all selected tokens - so remmber which one to get the right synthectic actor later.
    let token = MinorQOL._stateData.token || canvas.tokens.controlled[0];
    // When the target is placed finish the roll
    MinorQOL._stateData.targetHookId = Hooks.once("minor-qol-targeted", finishRoll.bind(null, {actor, item, event, token}));
    await template.drawPreview(event);
    // hide the character sheet if displayed
    if (actor.sheet.rendered || actor.sheet._state === Application.RENDER_STATES.RENDERING) actor.sheet.minimize();

    if (token?.actor && token?.actor?.sheet?.rendered) token.actor.sheet.minimize();
    // don't continue the roll until the template is placed
    return false;
  }
  // If we are going to use the default roll no need to display a card.
  if (!willUseStandardRoll && speedItemRolls === "onCard") await showItemCard( MinorQOL._stateData.cardItem);

  const targetDetails = item.data.data.target;
  if (rangeTarget && targetDetails && targetDetails.units === "ft" && ["creature", "ally", "enemy"].includes(targetDetails.type)) {
    const speaker = ChatMessage.getSpeaker();
    const token = canvas.tokens.get(speaker.token);
    if (!token) {
      ui.notifications.warn(`${game.i18n.localize("minor-qol.noSelection")}`)
      return true;
    }
      // We have placed an area effect template and we need to check if we over selected
    let dispositions = targetDetails.type === "creature" ? [-1,0,1] : targetDetails.type === "ally" ? [token.data.disposition] : [-token.data.disposition];
    // release current targets
    game.user.targets.forEach(t => {
      t.setTarget(false, { releaseOthers: false });
    });
    game.user.targets.clear();
    // calculate pixels eqivalent distance - requires map to be in ft.
    let minDist = targetDetails.value * canvas.grid.size / canvas.scene.data.gridDistance;
    canvas.tokens.placeables
          .filter(target => target.actor && target.actor.data.data.details.race !== "trigger"
                            && target.actor.id !== token.actor.id
                            && dispositions.includes(target.data.disposition) 
                            && Math.hypot(token.center.x - target.center.x, token.center.y - target.center.y) <= minDist)
          .forEach(token=> {
            token.setTarget(true, { user: game.user, releaseOthers: false });
            game.user.targets.add(token);
          });

    // Assumes area affect do not have a to hit roll
    MinorQOL._stateData.saves = new Set();
    MinorQOL._stateData.hitTargets = game.user.targets;
    game.user.broadcastActivity({targets: game.user.targets.ids});
  }

  // Filter targets according to disposition
  if (["ally", "enemy"].includes(item.data.data.target?.type)) {
    const speaker = ChatMessage.getSpeaker();
    const token = canvas.tokens.get(speaker.token);
    let mult = 0;
    switch (item.data.data.target.type) {
      case "ally": mult = token.data.disposition; break;
      case "enemy": mult = -token.data.disposition; break;
    }
   
    for (let target of game.user.targets) {
      if ((target.data.disposition * mult) <= 0) {
        target.setTarget(false, { user: game.user, releaseOthers: false });
        game.user.targets.delete(target);
      }
    }
  }
  return finishRoll({ actor, item, event});
};

/**
 * 
 * @param {Item, Event} Item that is rolling, event that generated the roll.
 * @param {*} targets optional list of targets to roll against
 */
let finishRoll = async ({ actor, item, event=null, token=null }, targets = null) => {
  // Spell has been cast if it is one.
  if (debug) console.warn("finishRoll", item, event, token);
  let result;
  // Next do the attack Roll

  // if a token is passed we need to take control of it - else we will get the wrong speaker sent to the damage card
  if (token) token.control({ releaseOthers: true });

  // from macros we get a mouse event so create a false event with the right shift key behaviour
  event = {
      altKey: event?.altKey,
      shiftKey: event?.shiftKey,
      metaKey: event?.metaKey,
      ctrlKey: event?.ctrlKey,
  };

  if (["all", "attack"].includes(autoShiftClick) && !(event.altKey || event.shiftKey || event.metaKey || event.ctrlKey)) {
      setShiftOnly(event);
  }

  if (item.hasAttack) {
    if (item.actor?.data.flags.dnd5e?.attackAdvantage < 0) {setCtrlOnly(event)} 
    else if (item.actor?.data.flags.dnd5e?.attackAdvantage > 0) {setAltOnly(event)} 
    // rollAttack consumes resources but not charges
    if (autoCheckHit === "snotty" && game.user.targets?.size === 0 && !item.hasAreaTarget) {
      ui.notifications.warn(i18n("minor-qol.abusePlayer"));
      return false;
    }
    return await item.rollAttack({ event, isCard: true});
    // return true;
  }

  if (item.hasDamage && (MinorQOL.forceRollDamage || autoRollDamage !== "none")) {
    // A Damage roll without an attck roll - need to account for resource consumption ourselves ?
    // let allowed = await item._handleResourceConsumption({isCard: true, isAttack: true});
    // if (!allowed) return false;
    if (["all", "damage"].includes(autoShiftClick) && !(event.altKey || event.shiftKey || event.metaKey || event.ctrlKey)) {
      setShiftOnly(event);
    }
    return item.rollDamage({ 
      event, 
      spellLevel: MinorQOL._stateData.spellLevel, 
      versatile: MinorQOL._stateData.versatile
    });
  } else if (item.hasDamage) {
     // we are not auto rolling damage and there was no attack roll
     // since nothing has been shown to the players unless speedItemRolls === "onCard"
     return await showItemCard( MinorQOL._stateData.cardItem);
  }


  if(item.hasSave) { // no damage/attack but still a save is required
    // A save roll without an attck or damage roll - need to account for resource consumption ourselves ?
    // let allowed = await item._handleResourceConsumption({isCard: true, isAttack: true});
    // if (!allowed) return false;
    if (autoCheckSaves !== "none") {
      MinorQOL._stateData.hitTargets = new Set(game.user.targets);
      await checkSaves(MinorQOL._stateData.hitTargets, item, ["gmroll", "blindroll"].includes(game.settings.get("core", "rollMode")));
      if (autoItemEffects && dynamicEffectsActive) 
        DynamicEffects.doEffects({item, actor: item.actor, activate: true, targets: MinorQOL._stateData.failedSaves, 
                                  whisper: true, spellLevel: MinorQOL._stateData.spellLevel});
      notifyComplete();
      resetStatus();
      return true;
    } else return await showItemCard( MinorQOL._stateData.cardItem);
  }

  if (item.type === "tool") {
    if (["all"].includes(autoShiftClick) && !(event.altKey || event.shiftKey || event.metaKey || event.ctrlKey)) {
      setShiftOnly(event);
    }
    result = await item.rollToolCheck({event});
  } else {
    // this wil call _handleResources & consume charges etc
    result = await item.roll({configureDialog: true});
  }
  // if we get here then thre is no attack/damage/save for the item. So the only thing that might happen is applying item effects
  if (autoItemEffects && dynamicEffectsActive && item.type !== "backpack") {
    let targets = (item?.data.data.target?.type === "self") ? getSelfTargetSet(actor) : game.user.targets
    DynamicEffects.doEffects({item, actor: item.actor, activate: true, targets, 
      whisper: true, spellLevel: MinorQOL._stateData.spellLevel}) 
  }
  notifyComplete();  
  resetStatus();
  return result;
};

let filterTargets = (removeAll = false) => {
  setTimeout(() => {
    if (autoRemoveTargets === "none") return;
    let newTargets = new Set();
    game.user.targets.forEach(t=> {
      if (t.actor.data.data.attributes.hp.value <= 0 || removeAll) {
        t.setTarget(false, { user: game.user, releaseOthers: false });
      } else {
        newTargets.add(t);
      }
    })
    game.user.targets = newTargets;
    MinorQOL._stateData.hitTargets = newTargets;
    // game.user.broadcastActivity({targets: game.user.targets.ids});
  }, (autoApplyDamage && autoRemoveTargets === "dead") ? 250 : 0); 
  // if removing dead and applying damage wait a bit for the gm client to do it's thing
}

let removeAllTargets = () => {
  game.user.targets.forEach(t=> {
      t.setTarget(false, { user: game.user, releaseOthers: false });
  })
  game.user.targets = new Set();
}

Hooks.on("renderChatMessage", (message, html) => {
  if (getProperty(message.data.flags, "minor-qol.waitForDiceSoNice") && !!diceSoNiceActive) {
    html.hide();
    Hooks.once("diceSoNiceRollComplete", (id) => {html.show(); ui.chat.scrollBottom()});
    setTimeout(() => {html.show(); ui.chat.scrollBottom()}, 3000); // backup display of messages
  }
  if (coloredBorders === "none") return true;
  html[0].style.borderColor = game.users.get(message.data.user).color;
  if (coloredBorders === "borderNames") html[0].children[0].children[0].style.backgroundColor = game.users.get(message.data.user).color;
  return true;
})

let processPreCreateAttackRoll =  async (data, options) => {
  let theTargets = game.user.targets;

  MinorQOL._stateData.isHit = true;
  let attackRoll, attackTotal;
  if (debug) log("preprocessAttackRoll |", speedItemRolls, autoCheckHit, autoApplyDamage)
  if (autoCheckHit === "none" && !MinorQOL.forceRollDamage && autoRollDamage === "none") {
    notifyComplete();  
    resetStatus();
    return;
  }
  attackRoll = Roll.fromData(JSON.parse(data.roll));
  attackTotal = attackRoll.total;
  MinorQOL._stateData.attackRoll = attackRoll;
  MinorQOL._stateData.attackTotal = attackTotal;
  let {actor, item} = getActorItemForMessage(data);

  // Assume we have a single die result
  if (isNewerVersion(game.data.version, "0.6.9")) {
    let rollValue = attackRoll.results[0];
    MinorQOL._stateData.isCritical = rollValue >= attackRoll.terms[0].options.critical;
    MinorQOL._stateData.isFumble = rollValue <= attackRoll.terms[0].options.fumble;
  } else {
    MinorQOL._stateData.isCritical = attackRoll.parts[0].results[0] >= attackRoll.parts[0].options.critical;
    MinorQOL._stateData.isFumble = attackRoll.parts[0].results[0] <= attackRoll.parts[0].options.fumble;
  }
  if (autoCheckHit !== "none") {
    let msg = "";
    let sep = "";
    
    // check for a hit/critical/fumble
    theTargets = new Set();
    let hitDisplay = [];
    const attackType = item?.name ? i18n(item.name) : "Attack";
    if (item?.data.data.target?.type === "self") {
      theTargets = getSelfTargetSet(actor);
    } else {
      for (let targetToken of game.user.targets) {
        MinorQOL._stateData.isHit = false;
        let targetName = useTokenNames && targetToken.name ? targetToken.name : targetToken.actor?.name;
        let targetActor = targetToken.actor;
        if (!targetActor) continue;
        let targetAC = targetActor.data.data.attributes.ac.value;
        if (!MinorQOL._stateData.isFumble && !MinorQOL._stateData.isCritical) {
            // check to see if the roll hit the target
            // let targetAC = targetActor.data.data.attributes.ac.value;
            MinorQOL._stateData.isHit = attackTotal >= targetAC;
            if (game.user.isGM) log(`${data.speaker.alias} Rolled a ${attackTotal} to hit ${targetName}'s AC of ${targetAC} is hit ${MinorQOL._stateData.isHit || MinorQOL._stateData.isCritical}`);
        }
        MinorQOL._stateData.extraText = `${data.speaker.alias} Rolled a ${attackTotal} to hit ${targetName}'s AC of ${targetActor.data.data.attributes.ac.value} ${MinorQOL._stateData.isCritical ? "(Critical)":""}`
        // Log the hit on the target
        let hitString = MinorQOL._stateData.isCritical ? i18n("minor-qol.criticals") : MinorQOL._stateData.isFumble? i18n("minor-qol.fumbles") : MinorQOL._stateData.isHit ? i18n("minor-qol.hits") : i18n("minor-qol.misses");
        let img = targetToken.data?.img || targetToken.actor.img;
        if ( VideoHelper.hasVideoExtension(img) ) {
          img = await game.video.createThumbnail(img, {width: 100, height: 100});
        }
        hitDisplay.push({isPC: targetToken.actor.isPC, target: targetToken, hitString, attackType, img});

        // If we hit and we have targets and we are applying damage say so.
        if (MinorQOL._stateData.isHit || MinorQOL._stateData.isCritical) theTargets.add(targetToken);
      }
    }
    MinorQOL._stateData.hitTargets = theTargets;
    const damageNotApplied = (autoApplyDamage === "none") 
    || theTargets?.size === 0 // no targets
    || (!MinorQOL.forceRollDamage && autoRollDamage === "none")  // we do not roll damage
    || (item?.hasSave && autoCheckSaves === "none") // it
    const templateData = {
      attackType: attackType,
      hits: hitDisplay, 
      isGM: game.user.isGM,
      damageAppliedString: damageNotApplied ? "" : i18n("minor-qol.damage-applied")
    }

    const content = await renderTemplate("modules/minor-qol/templates/hits.html", templateData);
    let speaker = ChatMessage.getSpeaker();
    speaker.alias = (useTokenNames && speaker.token) ? canvas.tokens.get(speaker.token).name : speaker.alias;

    if (game.user.targets.size > 0) {
      let chatData = {
        user: game.user._id,
        speaker,
        content: content,
        type: CONST.CHAT_MESSAGE_TYPES.OTHER,
        flags: { minorQolType: MESSAGETYPES.hitData }
      }
      if (autoCheckHit === "whisper" || data.whisper) 
      {
        chatData.whisper = ChatMessage.getWhisperRecipients("GM").filter(u=>u.active);
        chatData.user = ChatMessage.getWhisperRecipients("GM").find(u=>u.active);
      }
      setProperty(chatData.flags, "minor-qol.waitForDiceSoNice", !!!game.dice3d?.messageHookDisabled);
      ChatMessage.create(chatData);
    }
  }
  if (!item) item = MinorQOL._stateData.item; // for deleted items
  let shouldRollDamage = item.hasDamage && (autoRollDamage !== "none" || MinorQOL.forceRollDamage) && !MinorQOL._stateData.isFumble && (
                      ((game.user.targets.size === 0) || // nothing selected so roll damage if auto enabled
                      (autoRollDamage === "always") ||
                      (autoCheckHit === "none") || // we are not checking for hits so roll damage
                      (theTargets?.size > 0))); // we actually hit something
  if (debug) log("process attack roll - roll damage?", shouldRollDamage, item, item.hasDamage, autoRollDamage, game.user.targets.size, autoCheckHit, theTargets?.size);
  let event = {};

  if (["all", "damage"].includes(autoShiftClick) && (MinorQOL._stateData.isCritical || actor.data.flags.dnd5e?.forceCritical))
    setAltOnly(event);
  else if (["all", "damage"].includes(autoShiftClick) )
    setShiftOnly(event);
  
  if (shouldRollDamage) {
    // Chain the damage roll if required
    // wait for the chat message to appear before going on to roll damage
    Hooks.once("renderChatMessage", () => { 
      if (item.isSpell) {
        item.rollDamage({ 
          event, 
          spellLevel: MinorQOL._stateData.spellLevel, 
          versatile: MinorQOL._stateData.versatile
        });
      } else {
        item.rollDamage({ 
          event, 
          versatile: MinorQOL._stateData.versatile
        });

      }
      return true;
    })
  } else if (autoItemEffects && dynamicEffectsActive && theTargets?.size > 0 && !item?.hasDamage) { // no need to roll damage but perhaps apply item effects
    // assume effects only applied to hit targets
    DynamicEffects.doEffects({item, actor: item.actor, activate: true, targets: theTargets, whisper: true, spellLevel: MinorQOL._stateData.spellLevel});
  }
  if (!item.hasDamage) {
    notifyComplete();
    resetStatus();
  }
  return true;
};

function getSelfTargetSet(actor) {
  return new Set([getSelfTarget(actor)])
}
function getSelfTarget(actor) {
  if (actor.isPC) return actor.getActiveTokens()[0]; // if a pc always use the represented token
  const speaker = ChatMessage.getSpeaker()
  if (speaker.token) return canvas.tokens.get(speaker.token);
  if (actor.token) return actor.token;
  return undefined;
}

function getActorItemForMessage(messageData) {
  let actor = getActorForMessage(messageData);
  return {actor, item: getItemForMessage(messageData, actor)}
}
function getActorForMessage(messageData) {
  if (MinorQOL._stateData.actor) return MinorQOL._stateData.actor;
  let actor = game.actors.tokens[messageData.speaker.token];
  if (!actor) actor = game.actors.get(messageData.speaker.actor);
  return actor;
}
function getItemForMessage(messageData, actor) {
  // this is called after preCreate
  let item = MinorQOL._stateData.item;
  if (!item) item = actor?.items.find(i => messageData.flavor.startsWith(`${i.name}`) && (i.hasDamage || i.hasAttack));
  return item;
}

/**
 * If we roll an attack and we are not auto rolling damage add a roll damage button to the attack roll
 * If addDamageButtons add damage buttons to the roll
 * 
 * @param {ChatMessage} message 
 * @param {*} html 
 * @param {*  } data 
 */
let processAttackRoll = async (message, html, data) => {
  if (debug) log("processAttack", message, message.data.user);
  if (MinorQOL.forceRollDamage || autoRollDamage !== "none") return true;

  let {actor, item} = getActorItemForMessage(message.data)
  if (!item) return;
  if (speedItemRolls !== "onCard" || autoCheckHit === "none") {
    html.find(".message-content").append('<button class="minor-qol-roll-damage-button dnd5e chat-card card-buttons button" data-action="damage"> Roll Damage')
    let button = html.find(".minor-qol-roll-damage-button");
    button.off("click");
    // make a copy of spellLevel so it does not get overwritten.
    const spellLevel = MinorQOL._stateData.spellLevel;
    let versatile = MinorQOL._stateData.versatile;
    button.click(async (ev) => {
      ev.preventDefault();
      ev.stopPropagation();
      await item.rollDamage({event: ev, versatile, spellLevel});
      document.activeElement.blur();
      return true;
    });
  }
  return true;
  // <button data-action="damage">
}

// process a saving throw to see if it one we are waiting for.
let processSaveRoll = (message, html, data) => {
  const requestId = (lmrtfySupportsAttach && message.data.flags?.lmrtfy?.data) ? message.data.flags.lmrtfy.data : message.data.speaker.actor;
  if (!saveRequests[requestId]) return true;
  const total = message._roll._total;
  const formula = message._roll._formula;
  if (saveRequests[requestId]) {
    clearTimeout(saveTimeouts[requestId]);
    saveRequests[requestId]({total, formula})
    delete saveRequests[requestId];
    delete saveTimeouts[requestId];
  }      
  return true;
}

let processMQSaveRoll = async (message, html, data) => {
  return;
  if (debug) log("processSave", message, message.data.user);
  if (autoRollDamage !== "none") return true;
  let actor = game.actors.tokens[message.data.speaker.token];
  if (!actor) actor = game.actors.get(message.data.speaker.actor);
  // this is called after preCreate
  const itemRe = new RegExp(`\\s*(.*)\\s+DC[\\s0-9]*.*${i18n("minor-qol.saving-throw")}`)
  const itemName = message.data.flavor.match(itemRe);
  if (!itemName) {
    console.warn("Minor-qol | could not find item name", itemName, itemRe)
    return;
  }
  const item = actor.items.find(i => i.name === itemName[1] && i.hasDamage);
  //if (!item) item = MinorQOL._stateData.item;
  if (!item?.hasDamage) return;
  html.find(".message-content").append('<button class="minor-qol-roll-damage-button dnd5e chat-card card-buttons button" data-action=""> Roll Damage')
  let button = html.find(".minor-qol-roll-damage-button");
  button.off("click");
  button.click(async (ev) => {
        ev.stopPropagation();
        const saveAutoCheckSaves = autoCheckSaves;
        autoCheckSaves = "none";
        await item.rollDamage({event: ev, versatile: MinorQOL._stateData.versatile})
        Hooks.once("renderChatMessage", () => {
          autoCheckSaves = saveAutoCheckSaves
        });
  });
  return true;

  // <button data-action="damage">
}
/**
 * If autoApplyDamge is set apply the damage to minor-qol.hitTargets using immunities and saving throws if appropriate.
 * If addDamageButtons add damage buttons to the roll
 * 
 * @param {ChatMessage} message 
 * @param {*} html 
 * @param {*  } data 
 */
let processDamageRoll = async (message, html, data) => {
  if (debug) log("processDamageRoll", message, message.data.user);
  // proceed if adding chat damage buttons or applying damage for our selves
  if (!addChatDamageButtons && (autoApplyDamage === "none" || message.data.user !==  game.user._id)) {
    if (autoApplyDamage === "none") {
      MinorQOL._stateData.damageTotal = message.roll.total;
      notifyComplete();
      resetStatus();
    }
    return true;
  }
  let appliedDamage = 0;
  const {actor, item} = getActorItemForMessage(message.data);
  MinorQOL._stateData.damageTotal = message.roll.total;
  const re = /.*\((.*)\)/;
  const defaultDamageType = message.data.flavor && message.data.flavor.match(re);
  MinorQOL._stateData.damageRoll = message.roll;
  MinorQOL._stateData.damageList = createDamageList(message.roll, item, defaultDamageType?.length > 1 ? defaultDamageType[1].toLowerCase() : "radiant");
  // Show damage buttons if enabled, but only for the applicable user and the GM
  if ((message.user.id === game.user.id || game.user.isGM) && addChatDamageButtons) {
    addDamageButtons(MinorQOL._stateData.damageList, MinorQOL._stateData.damageTotal, html);
  }
  if (message.user.id !== game.user.id) return true;
  let theTargets = game.user.targets;

  if (item?.hasAttack) theTargets = MinorQOL._stateData.hitTargets;
  if (item?.data.data.target?.type === "self") theTargets = getSelfTargetSet(actor) || theTargets;
  if (!theTargets?.size) {
    MinorQOL._stateData.theTargets = theTargets;
    notifyComplete();
    return;
  }
  checkSaves(theTargets, item, data.message.whisper?.length > 0).then((saves) => {
    saves = MinorQOL._stateData.saves;
    if (!item?.hasSave || autoCheckSaves !== "none") { // if the item has a save but we are not checking them don't apply damage
      if (autoApplyDamage !== "none" && game.user._id === message.data.user) {
        appliedDamage = applyTokenDamage(MinorQOL._stateData.damageList, MinorQOL._stateData.damageTotal, theTargets, item, saves);
      }
      if (item && autoItemEffects && !message.data.flags?.noDynamicEffects && dynamicEffectsActive && message.data.user ===  game.user._id 
        && theTargets?.size) { // perhaps apply item effects
        // If we rolled a save update theTargets
        if (autoCheckSaves !== "none" && item?.hasSave) theTargets = MinorQOL._stateData.failedSaves;
        // assume effects only applied to hit targets
        if (autoCheckHit !== "none" || (autoCheckSaves !== "none" && theTargets.size > 0)) {
          if (debug) log("processDamage Roll - about to call doEffects ", item, item.actor, theTargets)
          let spellLevel = MinorQOL._stateData.spellLevel;
          DynamicEffects.doEffects({item, actor: item.actor, activate: true, targets: theTargets, 
                                  whisper: true, spellLevel, damageTotal: theTargets.size > 1 ? MinorQOL._stateData.damageTotal : appliedDamage}) 
        }
      }
    }
    notifyComplete();
    resetStatus();
    filterTargets();   
  });
  return true;
};

let processUndoDamageCard = async(message, html, data) => {
  message.data.flags["minor-qol"] && message.data.flags["minor-qol"].forEach(({tokenID, oldTempHP, oldHP}) => {
    let token = canvas.tokens.get(tokenID);
    if (!token) {
      log(`Token ${tokenID} not found`);
      return;
    }
    let button = html.find(`#${tokenID}`);
    button.click(async (ev) => {
      log(`Setting HP back to ${oldTempHP} and ${oldHP}`);
      let actor = canvas.tokens.get(tokenID).actor;
      await actor.update({ "data.attributes.hp.temp": oldTempHP, "data.attributes.hp.value": oldHP });
      ev.stopPropagation();
    });
  })
}
/**
 * 
 * @param {Set} theTargets 
 * @param {Item} item 
 * 
 * Return a Set of successful saves from the set of tokens theTargets.
 */
let checkSaves = async (theTargets, item, whisper = false) => {
    MinorQOL._stateData.saves = new Set();
    if (autoCheckSaves === "none") return  MinorQOL._stateData.saves;
    MinorQOL._stateData.failedSaves = new Set()
    let saveDisplay = [];
    /*
    if (!theTargets?.size) {
        theTargets = game.user.targets;
        MinorQOL._stateData.hitTargets = game.user.targets;
    }*/
    if (item?.hasSave && theTargets.size > 0) {
      // requestPCSave(item.data.data.save.ability);

        let rollDC = item.data.data.save.dc;
        let rollAbility = item.data.data.save.ability;
        let message = "";
        let promises = [];

        MinorQOL._stateData.saveCount = autoCheckSaves !== "allShow" ? theTargets.size : 0;
        // make sure saving throws are renabled.
        try {
          saveRequests = {}
          MinorQOL._stateData.hideSaves = autoCheckSaves !== "allShow";
          for (let target of theTargets) {
            if (!target.actor) { // no actor means multi levels or bugged actor - but we won't roll a save
              MinorQOL._stateData.saveCount -= 1;
              continue;
            }
            let advantage = false;
            // If spell, check for magic resistance
            if (item.data.type === "spell") {
              // check magic resistance in custom damage reduction traits
              advantage = (target?.actor?.data?.data?.traits?.dr?.custom || "").includes(i18n("minor-qol.MagicResistant"));
              // check magic resistance as a feature (based on the SRD name as provided by the DnD5e system)
              advantage = advantage || (target?.actor?.data?.items?.filter(a => a.type==="feat" && a.name===i18n("minor-qol.MagicResistanceFeat")).length > 0);
              if (debug) log(`minor-qol | ${target.actor.name} resistant to magic : ${advantage}`);
            }
            let event = {};
            if (advantage) setAltOnly(event)
            else setShiftOnly(event)

            if (playerRollSaves !== "none") { // find a player to send the request to
              // find the controlling player
              var player = game.users.players.find(p=> p.character?._id === target.actor._id);
              if (!player?.active) { // no controller - find the first owner who is active
                player = game.users.players.find(p=>p.active && target.actor.data.permission[p._id] === CONST.ENTITY_PERMISSIONS.OWNER)
                // player = game.user.players.find(p => p.active && target.actor.permission[p.id] === CONST.ENTITY_PERMISSIONS.OWNER);
              }
            }
            if (playerRollSaves !== "none" && player?.active) {
              MinorQOL._stateData.saveCount = Math.max(MinorQOL._stateData.saveCount - 1, 0)
              log(`Player ${player.name} controls actor ${target.actor.name} - requesting ${CONFIG.DND5E.abilities[item.data.data.save.ability]} save`);
              promises.push(new Promise((resolve, reject) => {
                let eventToUse = duplicate(event);
                let advantageToUse = advantage;
                let requestId = target.actor.id;
                let playerName = player.name;
                if (["letem", "letmeQuery"].includes(playerRollSaves) && lmrtfySupportsAttach) requestId = randomID();
                saveRequests[requestId] = resolve;
                requestPCSave(item.data.data.save.ability, player.id, target.actor.id, advantage, item.name, rollDC, requestId)
                // set a timeout for taking over the roll
                saveTimeouts[requestId] = setTimeout(async () => {
                  console.warn(`minor-qol | Timeout waiting for ${playerName} to roll ${CONFIG.DND5E.abilities[item.data.data.save.ability]} save - rolling for them`)
                  if (saveRequests[requestId]) {
                      delete saveRequests[requestId];
                      delete saveTimeouts[requestId];
                      let result = await target.actor.rollAbilitySave(item.data.data.save.ability, {event: eventToUse, advantage: advantageToUse});
                      resolve(result);
                  }
                }, playerSaveTimeout * 1000);
              }))
            } else {
              promises.push(target.actor.rollAbilitySave(item.data.data.save.ability, {event, advantage}));
            }
          }
        } catch (err) {
            console.warn(err)
        } finally {
          MinorQOL._stateData.hideSaves = MinorQOL._stateData.saveCount > 0;
        }
        var results = await Promise.all(promises);

        saveRequests = {};
        if (debug) log("Results are ", results);
        let i = 0;
        for (let target of theTargets) {
          if (!target.actor) continue;
            let rollTotal = results[i].total;
            let saved = rollTotal >= rollDC;
            if (saved)
              MinorQOL._stateData.saves.add(target);
            else
              MinorQOL._stateData.failedSaves.add(target);
            if (game.user.isGM) log(`Ability save result is ${target.name} rolled ${rollTotal} vs ${rollAbility} DC ${rollDC}`);
            let saveString = i18n(saved ? "minor-qol.save-success" : "minor-qol.save-failure");
            let noDamage = saved && getSaveMultiplierForItem(item) === 0 ? i18n("minor-qol.noDamage") : "";
            let adv = results[i].formula.includes("kh") ? `(${i18n("minor-qol.advantage")})` : "";
            let img = target.data.img || target.actor.img;
            if ( VideoHelper.hasVideoExtension(img) ) {
              img = await game.video.createThumbnail(img, {width: 100, height: 100});
            }
            saveDisplay.push({name: target.name, img, isPC: target.actor.isPC, target, saveString, rollTotal, noDamage, id: target.id, adv});
            i++;
        }
        let templateData = {
          saves: saveDisplay, 
          damageAppliedString: (MinorQOL.forceRollDamage || autoRollDamage !== "none") && autoApplyDamage !== "none" && item.hasDamage ? i18n("minor-qol.damage-applied") : ""
         }
        let content = await renderTemplate("modules/minor-qol/templates/saves.html", templateData);
        let speaker = ChatMessage.getSpeaker();
        speaker.alias = (useTokenNames && speaker.token) ? canvas.tokens.get(speaker.token).name : speaker.alias;

        let chatData = {
          user: game.user._id,
          speaker,
          content: `<div data-item-id="${item._id}"></div> ${content}`,
          flavor: `<h4"> ${item.name} DC ${rollDC} ${CONFIG.DND5E.abilities[rollAbility]} ${i18n(theTargets.size > 1 ? "minor-qol.saving-throws" : "minor-qol.saving-throw")}:</h4>`,
          type: CONST.CHAT_MESSAGE_TYPES.OTHER,
          flags: { minorQolType: MESSAGETYPES.saveData }
        };
        if (autoCheckSaves === "whisper" || whisper) {
          chatData.whisper = ChatMessage.getWhisperRecipients("GM").filter(u=>u.active);
          chatData.user = ChatMessage.getWhisperRecipients("GM").find(u=>u.active)
        }
        setProperty(chatData.flags, "minor-qol.waitForDiceSoNice", !!!game.dice3d?.messageHookDisabled);
        await ChatMessage.create(chatData);
    }
    return MinorQOL._stateData.saves;
};

// Calculate the hp/tempHP lost for an amount of damage of type
function calculateDamage(a, appliedDamage, t, totalDamage, dmgType) {
  let value = Math.floor(appliedDamage);
  if (dmgType.includes("temphp")) { // only relavent for healing of tmp HP
    var hp = a.data.data.attributes.hp;
    var tmp = parseInt(hp.temp) || 0;
    var oldHP = hp.value;
    var newTemp = Math.max(tmp - value, 0);
    var newHP = hp.value;
  } else {
    var hp = a.data.data.attributes.hp, tmp = parseInt(hp.temp) || 0, dt = value > 0 ? Math.min(tmp, value) : 0;
    var newTemp = tmp - dt;
    var oldHP = hp.value;
    var newHP = Math.clamped(hp.value - (value - dt), 0, hp.max + (parseInt(hp.tempmax)|| 0));
  }

  if (game.user.isGM)
      log(`${a.name} takes ${value} reduced from ${totalDamage} Temp HP ${newTemp} HP ${newHP}`);
  return {tokenID: t.id, actorID: a._id, tempDamage: dt, hpDamage: oldHP - newHP, oldTempHP: tmp, newTempHP: newTemp,
          oldHP: oldHP, newHP: newHP, totalDamage: totalDamage, appliedDamage: value};
}


/** 
 * Work out the appropriate multiplier for DamageTypeString on actor
 * If DamageImmunities are not being checked always return 1
 * 
 */

let getTraitMult = (actor, dmgTypeString) => {
  if (dmgTypeString.includes("healing") || dmgTypeString.includes("temphp")) return -1;
  if (damageImmunities !== "none") {
    if (dmgTypeString !== "") {
      if (actor.data.data.traits.di.value.some(t => dmgTypeString.includes(t))) return 0;
      // if (actor.data.data.traits.di.custom.includes(dmgTypeString)) return 0;
      if (actor.data.data.traits.dr.value.some(t => dmgTypeString.includes(t))) return 0.5;
      // if (actor.data.data.traits.dr.custom.includes(dmgTypeString)) return 0.5;
      if (actor.data.data.traits.dv.value.some(t => dmgTypeString.includes(t))) return 2;
      // if (actor.data.data.traits.dv.custom.includes(dmgTypeString)) return 2;
    }
  }
  // Check the custom immunities
  return 1;
};

let _highlighted = null;

let _onTargetHover = (event) => {
  event.preventDefault();
  if ( !canvas.scene.data.active ) return;
//  const li = event.currentTarget;
//  const token = canvas.tokens.get(li.id);
  const token = canvas.tokens.get(event.currentTarget.id);
  if ( token?.isVisible ) {
    if ( !token._controlled ) token._onHoverIn(event);
    _highlighted = token;
  }
}

/* -------------------------------------------- */

/**
 * Handle mouse-unhover events for a combatant in the tracker
 * @private
 */
let _onTargetHoverOut = (event) => {
  event.preventDefault();
  if ( !canvas.scene.data.active ) return;
  if (_highlighted ) _highlighted._onHoverOut(event);
  _highlighted = null;
}

let _onTargetSelect = (event) => {
  event.preventDefault();
  if ( !canvas.scene.data.active ) return;
  const token = canvas.tokens.get(event.currentTarget.id);
  token.control({ multiSelect: false, releaseOthers: true });
};


/**
 * If this is a minor qol roll (save or hits) make the actor name buttons select the token for the GM.
 */
let setupHiddenNameButtons = () => {
  Hooks.on("renderChatMessage", (message, html, data) => {
    if (![MESSAGETYPES.hitData, MESSAGETYPES.saveData].includes(getProperty(message.data.flags, "minorQolType")))
      return;
    let ids = html.find(".minor-qol-target-name")
    // let buttonTargets = html.getElementsByClassName("minor-qol-target-npc");

    ids.hover(_onTargetHover, _onTargetHoverOut)

    if (game.user.isGM)  {
      ids.click(_onTargetSelect);
    }
    if (!game.user.isGM && hideNPCNames) {
      ids=html.find(".minor-qol-target-npc");
      ids.text(hiddenName);
    }
  });
}


/**
 *  return a list of {damage: number, type: string} for the roll and the item
 */
let createDamageList = (roll, item, defaultType = "radiant") => {
  if (isNewerVersion(game.data.version, "0.6.9")) {
    let damageList = [];
    let rollTerms = roll.terms;
    let partPos = 0;
    let evalString;
    let damageSpec = item ? item.data.data.damage : {parts: []};
    if (debug) log("Passed roll is ", roll)
    if (debug) log("Damage spec is ", damageSpec)
    for (let [spec, type] of damageSpec.parts) {
      if (debug) log("single Spec is ", spec, type, item)
      if (item) {
        var rollSpec = new Roll(spec, item.actor?.getRollData() || {}).roll();
      }
      if (debug) log("rollSpec is ", spec, rollSpec)
      let specLength = rollSpec.terms.length;
      evalString = "";

      if (debug) log("Spec Length ", specLength, rollSpec.terms)
      for (let i = 0; i < specLength && partPos < rollTerms.length; i++) {
        if (typeof rollTerms[partPos] !== "object") {
          evalString += rollTerms[partPos];
        } else {
          if (debug) log("roll parts ", rollTerms[partPos])
          let total = rollTerms[partPos].total;
          evalString += total;
        }
        partPos += 1;
      }
      let damage = new Roll(evalString).roll().total;
      if (debug) log("Damage is ", damage, type, evalString)
      damageList.push({ damage: damage, type: type });
      partPos += 1; // skip the plus
    }
    if (debug) log(partPos, damageList)
    evalString = "";
    while (partPos < rollTerms.length) {
      if (debug) log(rollTerms[partPos])
      if (typeof rollTerms[partPos] === "object") {
        let total = rollTerms[partPos].total;
        evalString += total;
      }
      else evalString += rollTerms[partPos];
      partPos += 1;
    }
    if (evalString.length > 0) {
      if (debug) log("Extras part is ", evalString)
        let damage = new Roll(evalString).roll().total;
        let type = damageSpec.parts[0] ? damageSpec.parts[0][1] : defaultType;
        damageList.push({ damage, type});
        if (debug) log("Extras part is ", evalString)
    }
    if (debug) log("Final damage list is ", damageList)
    return damageList;
  } else {
    debug=true;
    let damageList = [];
    let rollParts = roll.parts;
    let partPos = 0;
    let evalString;
    let damageSpec = item ? item.data.data.damage : {parts: []};
    if (debug) log("Passed roll is ", roll)
    if (debug) log("Damage spec is ", damageSpec)
    for (let [spec, type] of damageSpec.parts) {
      if (debug) log("single Spec is ", spec, type, item)
      if (item) {
        var rollSpec = new Roll(spec, item.actor?.getRollData() || {}).roll();
      }
      if (debug) log("rollSpec is ", spec, rollSpec)
      let specLength = rollSpec.parts.length;
      evalString = "";
  
      if (debug) log(specLength, rollSpec.parts)
      for (let i = 0; i < specLength && partPos < rollParts.length; i++) {
        if (typeof rollParts[partPos] === "object") {
          if (debug) log("roll parts ", rollParts[partPos])
          let total = rollParts[partPos].total;
          evalString += total;
        }
        else evalString += rollParts[partPos];
        partPos += 1;
      }
      let damage = new Roll(evalString).roll().total;
      if (debug) log("Damage is ", damage, type, evalString)
      damageList.push({ damage: damage, type: type });
      partPos += 1; // skip the plus
    }
    if (debug) log(partPos, damageList)
    evalString = "";
    while (partPos < rollParts.length) {
      if (debug) log(rollParts[partPos])
      if (typeof rollParts[partPos] === "object") {
        let total = rollParts[partPos].total;
        evalString += total;
      }
      else evalString += rollParts[partPos];
      partPos += 1;
    }
    if (evalString.length > 0) {
      if (debug) log("Extras part is ", evalString)
        let damage = new Roll(evalString).roll().total;
        let type = damageSpec.parts[0] ? damageSpec.parts[0][1] : "radiant";
        damageList.push({ damage, type});
        if (debug) log("Extras part is ", evalString)
    }
    if (debug) log("Final damage list is ", damageList)
    return damageList;
  }
  debug=false;
};

let applyTokenDamage = (damageList, totalDamage, theTargets, item, saves) => {
    let appliedDamageList = [];
    let targetNames = [];
    let appliedDamage;
    if (debug) log("applyTokenDamage - targets", theTargets)
    if (!theTargets || theTargets.size === 0) {
      if (debug) console.warn("applyTokenDamage - targets", theTargets)

      // probably called from refresh - don't do anything
      resetStatus();
      return true;
    }
    for (let t of theTargets) {
        let a = t.actor;
        if (!a) continue;
        appliedDamage = 0;
        for (let { damage, type } of damageList) {
            let mult = saves.has(t) ? getSaveMultiplierForItem(item) : 1;
            mult = mult * getTraitMult(a, type);
            appliedDamage += Math.floor(damage * Math.abs(mult)) * Math.sign(mult);
            var dmgType = type;
          }
          appliedDamageList.push(calculateDamage(a, appliedDamage, t, totalDamage, dmgType));
        targetNames.push(t.name)
    }
    if (theTargets.size > 0) {
      let intendedGM = game.user.isGM ? game.user : game.users.entities.find(u => u.isGM && u.active);
      if (!intendedGM) {
        ui.notifications.error(`${game.user.name} ${i18n("minor-qol.noGM")}`);
        console.error("Minor Qol | No GM user connected - cannot apply damage");
        return;
      }

      broadcastData({
            action: "reverseDamageCard",
            sender: game.user.name,
            intendedFor: intendedGM.id,
            damageList: appliedDamageList,
            settings: getParams(),
            targetNames,
            extraText: MinorQOL._stateData.extraText
        });
    }
    return appliedDamage;
};

let addDamageButtons = async (damageList, totalDamage, html) => {
    const btnContainer = $('<span class="dmgBtn-container" style="position:absolute; right:0; bottom:1px;"></span>');
    let btnStyling = "width: 22px; height:22px; font-size:10px;line-height:1px";
    const fullDamageButton = $(`<button class="dice-total-full-damage-button" style="${btnStyling}"><i class="fas fa-user-minus" title="Click to apply full damage to selected token(s)."></i></button>`);
    const halfDamageButton = $(`<button class="dice-total-half-damage-button" style="${btnStyling}"><i class="fas fa-user-shield" title="Click to apply half damage to selected token(s)."></i></button>`);
    const doubleDamageButton = $(`<button class="dice-total-double-damage-button" style="${btnStyling}"><i class="fas fa-user-injured" title="Click to apply double damage to selected token(s)."></i></button>`);
    const fullHealingButton = $(`<button class="dice-total-full-healing-button" style="${btnStyling}"><i class="fas fa-user-plus" title="Click to apply full healing to selected token(s)."></i></button>`);
    btnContainer.append(fullDamageButton);
    btnContainer.append(halfDamageButton);
    btnContainer.append(doubleDamageButton);
    btnContainer.append(fullHealingButton);
    html.find(".dice-total").append(btnContainer);
    // Handle button clicks
    let setButtonClick = (buttonID, mult) => {
        let button = html.find(buttonID);
        button.off("click");
        button.click(async (ev) => {
            ev.stopPropagation();
            if (canvas.tokens.controlled.length === 0) {
                console.warn(`Minor-qol | user ${game.user.name} ${i18n("minor-qol.noTokens")}`);
                return ui.notifications.warn(`${game.user.name} ${i18n("minor-qol.noTokens")}`);
            }
            // find solution for non-magic weapons
            let promises = [];
            for (let t of canvas.tokens.controlled) {
                let a = t.actor;
                let appliedDamage = 0;
                for (let { damage, type } of damageList) {
                    let typeMult = mult * Math.abs(getTraitMult(a, type)); // ignore damage type for buttons
                    appliedDamage += Math.floor(Math.abs(damage * typeMult)) * Math.sign(typeMult);
                }
                let damageItem = calculateDamage(a, appliedDamage, t, totalDamage, "");
                promises.push(a.update({ "data.attributes.hp.temp": damageItem.newTempHP, "data.attributes.hp.value": damageItem.newHP }));
            }
            let retval = await Promise.all(promises);
            return retval;
        });
    };
    setButtonClick(".dice-total-full-damage-button", 1);
    setButtonClick(".dice-total-half-damage-button", 0.5);
    setButtonClick(".dice-total-double-damage-button", 2);
    setButtonClick(".dice-total-full-healing-button", -1);
};

let itemDeleteHandler = ev => {
  let actor = game.actors.get(ev.data.data.actor._id);
  let d = new Dialog({
      // localize this text
      title: i18n("minor-qol.reallyDelete"),
      content: `<p>${i18n("minor-qol.sure")}</p>`,
      buttons: {
          one: {
              icon: '<i class="fas fa-check"></i>',
              label: "Delete",
              callback: () => {
                  let li = $(ev.currentTarget).parents(".item"), itemId = li.attr("data-item-id");
                  ev.data.app.object.deleteOwnedItem(itemId);
                  li.slideUp(200, () => ev.data.app.render(false));
              }
          },
          two: {
              icon: '<i class="fas fa-times"></i>',
              label: "Cancel",
              callback: () => { }
          }
      },
      default: "two"
  });
  d.render(true);
};

let setShiftOnly = event => {
    event.shiftKey = true;
    event.altKey = false;
    event.ctrlKey = false;
    event.metaKey = false;
};
let setAltOnly = event => {
    event.shiftKey = false;
    event.altKey = true;
    event.ctrlKey = false;
    event.metaKey = false;
};
let setCtrlOnly = event => {
  event.shiftKey = false;
  event.altKey = false;
  event.ctrlKey = true;
  event.metaKey = false;
};

let moduleSocket = "module.minor-qol";

let createReverseDamageCard = async (data) => {
  if (data.intendedFor === game.user.id) {
    let whisperText = "";
    const damageList = data.damageList;
    const btnStyling = "width: 22px; height:22px; font-size:10px;line-height:1px";
    let token, actor;
    const timestamp = Date.now();
    let sep = "";
    let promises = [];
    let tokenIdList = [];
    for (let { tokenID, actorID, tempDamage, hpDamage, totalDamage, appliedDamage } of damageList) {
        token = canvas.tokens.get(tokenID);
        actor = token.actor;
        const hp = actor.data.data.attributes.hp;
        var oldTempHP = hp.temp;
        var oldHP = hp.value;

        tokenIdList.push({tokenID, oldTempHP: oldTempHP, oldHP: hp.value});
        if (tempDamage > oldTempHP) {
          var newTempHP = 0;
          hpDamage += (tempDamage - oldTempHP)
        } else {
          var newTempHP = oldTempHP - tempDamage;
        }
        let newHP = Math.max(0, actor.data.data.attributes.hp.value - hpDamage);
        promises.push(actor.update({ "data.attributes.hp.temp": newTempHP, "data.attributes.hp.value": newHP }));
        let buttonID = `${token.id}`;
        let btntxt = `<button id="${buttonID}"style="${btnStyling}"><i class="fas fa-user-plus" title="Click to reverse damage."></i></button>`;
        let tokenName = token.name && useTokenNames ? `<strong>${token.name}</strong>` : token.actor.name;
        let dmgSign = appliedDamage < 0 ? "+" : "-"; // negative damage is added to hit points
        if (oldTempHP > 0)
            whisperText = whisperText.concat(`${sep}${duplicate(btntxt)} ${tokenName}<br> (${oldHP}:${oldTempHP}) ${dmgSign} ${Math.abs(appliedDamage)}[${totalDamage}] -> (${newHP}:${newTempHP})`);
        else
            whisperText = whisperText.concat(`${sep}${duplicate(btntxt)} ${tokenName}<br> ${oldHP} ${dmgSign} ${Math.abs(appliedDamage)}[${totalDamage}] -> ${newHP}`);
        ["di", "dv", "dr"].forEach(trait => {
          let traits = actor.data.data.traits[trait]
          if (traits.custom || traits.value.length > 0) {
            whisperText = whisperText.concat(`<br>${traitList[trait]}: ${traits.value.map(t=>CONFIG.DND5E.damageTypes[t]).concat(traits.custom)}`);
          }
        });
        sep = "<br>";
    }
    let results =  await Promise.allSettled(promises);

    const speaker = ChatMessage.getSpeaker();
    speaker.alias = game.user.name;
    if (autoApplyDamage === "yesCard") {
      let chatData = {
        user: game.user._id,
        speaker,
        content: whisperText,
        whisper: ChatMessage.getWhisperRecipients("GM").filter(u=>u.active),
        flavor: `${i18n("minor-qol.undoDamageFrom")} ${data.sender}`,
        type: CONST.CHAT_MESSAGE_TYPES.OTHER,
        flags: {"minor-qol": tokenIdList}
      };
      if (debugDamageApply) {
        chatData.content = chatData.content + `<br>${data.settings} <br>${data.targetNames} <br> ${data.extraText}`
      }
      let message = await ChatMessage.create(chatData);
    }
  }
};

let processAction = async data => {
    switch (data.action) {
        case "reverseDamageCard":
            if (!game.user.isGM)
                break;
            if (autoApplyDamage == "none")
                break;
            await createReverseDamageCard(data);
            break;
    }
};
let setupSocket = () => {
    game.socket.on(moduleSocket, data => {
        processAction(data);
    });
};

function broadcastData(data) {
    // if not a gm broadcast the message to a gm who can apply the damage
    if (game.user.id !== data.intendedFor)
      game.socket.emit(moduleSocket, data, resp => { });
    else
      processAction(data);
}

let getSaveMultiplierForItem = item => {
  // find a better way for this ? perhaps item property
  if (!item) return 1;
  if (noDamageSaves.includes(cleanSpellName(item.name))) return 0;
  if (item.data.data.description.value.includes(i18n("minor-qol.noDamageText"))) {
    return 0.0;
  } 
  if (damageImmunities === "savesDefault") return 0.5;
  if (item.data.data.description.value.includes(i18n("minor-qol.halfDamage")) || item.data.data.description.value.includes(i18n("minor-qol.halfDamageAlt"))) {
    return 0.5;
  }
  //  Think about this. if (damageImmunities !== "savesDefault" && item.hasSave) return 0; // A save is specified but the half-damage is not specified.
  return 1;
};

let i18n = key => {
  return game.i18n.localize(key);
};

knownSheets = {
  BetterNPCActor5eSheet: ".item .rollable",
  ActorSheet5eCharacter: ".item .item-image",
  BetterNPCActor5eSheetDark: ".item .rollable",
  ActorSheet5eCharacterDark: ".item .item-image",
  DarkSheet: ".item .item-image",
  ActorNPC5EDark: ".item .item-image",
  DynamicActorSheet5e: ".item .item-image",
  ActorSheet5eNPC: ".item .item-image",
  DNDBeyondCharacterSheet5e: ".item .item-name .item-image",
  Tidy5eSheet:  ".item .item-image",
  Tidy5eNPC: ".item .item-image",
  MonsterBlock5e: ".item .item-name",
  CompactBeyond5eSheet: ".item .item-image",

//  Sky5eSheet: ".item .item-image",
};

let tokenScene = (tokenName) => {
  for (let scene of game.scenes.entities) {
    let token = scene.data.tokens.find(t=> t.name === tokenName);
    if (token) return {scene, token};
  }
  return null;
}

function requestPCSave(ability, playerId, actorId, advantage, flavor, dc, requestId) {
 if (lmrtfyActive && ["letme", "letmeQuery"].includes(playerRollSaves)) {
    const socketData = {
        user: playerId,
        actors: [actorId],
        abilities: [],
        saves: [ability],
        skills: [],
        advantage: playerRollSaves === "letmeQuery"? 2 : (advantage ? 1 : 0),
        mode: "roll",
        title: "You need to save ...",
        message: `DC ${dc} save against ${flavor}`,
        formula: "",
        attach: requestId,
        deathsave: false,
        initiative: false
    }
    game.socket.emit('module.lmrtfy', socketData);
    LMRTFY.onMessage(socketData);
 } else {
   let player = game.users.get(playerId);
   let actorName = game.actors.get(actorId).name;
  ChatMessage.create({
    content: ` ${actorName} Roll DC ${dc} ${CONFIG.DND5E.abilities[ability]} saving throw${advantage ? " with advantage" : ""} against ${flavor}`,
    whisper: [player]
  });
 }
}

/**
 * 
 * @param {Item} item 
 * @param {integer} slot 
 * 
 * If no macro exists create a macro roll
 */
async function createMinorQOLMacro(item, slot) {
  const command = `MinorQOL.doRoll(event, "${item.name}", {type: "${item.type}", versatile: false});`;
  let macro = game.macros.entities.find(m => m.name.startsWith(item.name)  &&  m.data.command === command);
  if (!macro) {
      macro = await Macro.create({
          name: `${item.name} - ${item.type}`,
          type: "script",
          img: item.img,
          command: command,
          flags: { "dnd5e.itemMacro": true }
      }, { displaySheet: false });
  }
  game.user.assignHotbarMacro(macro, slot);
}

export function doMacroRoll(event, itemName, itemType = "weapon") {
return doRoll(event, itemName, {type: itemType});
}

export async function doRoll(event, itemName, {type = "weapon", versatile=false, token = null}={type:"weapon", versatile: false, token: null}) {
  if (!macroSpeedRolls) return game.dnd5e.rollItemMacro(itemName);
  // Get the syntehtic actor if there is one
  if (token) {
    var actor = typeof token === "string" ? canvas.tokens.get(token).actor : token.actor;
  } else {
    var speaker = ChatMessage.getSpeaker();
    var token = canvas.tokens.get(speaker.token);
    var actor = token ? token.actor : game.actors.get(speaker.actor);
  }
  if (!actor) {
    ui.notifications.warn(`${game.i18n.localize("minor-qol.noSelection")}`)
    return;
  } 
  if (!event) {
    event = {altKey: false, shiftKey: true, metaKey: false, ctrlKey: false, originalEvent: null};
  } else if (!event.originalEvent) {
    // from macros we get a mouse event so create a false event with the right shift key behaviour
    event = {altKey: event.altKey, shiftKey: event.shiftKey, metaKey: event.metaKey, ctrlKey: event.ctrlKey, originalEvent: event};
  }
  let item = actor?.items?.get(itemName) // see if we got an itemId
  if (!item) item = actor?.items?.find(i => i.name === itemName && (!type || i.type === type));
  if (!item) return ui.notifications.warn(`${i18n("minor-qol.noItemNamed")} ${itemName}`);
  // Check pre roll checks
  if (preRollChecks && Hooks.call("MinorQolPreItemRoll", actor, item, event) === false) {
    console.warn("Minor-qol | item roll blocked by pre checks");
    return;
  }
  return await doCombinedRoll({actor, item, event, versatile, token});
}

let showItemCard = async (item) => {
  const token = item.actor.token;
  const templateData = {
    actor: item.actor,
    tokenId: token ? `${token.scene._id}.${token.id}` : null,
    item: item.data,
    data: item.getChatData(),
    labels: item.labels,
    hasAttack: false,
    isHealing: item.isHealing && autoRollDamage === "none",
    hasDamage: item.hasDamage && autoRollDamage === "none",
    isVersatile: item.isVersatile && autoRollDamage === "none",
    isSpell: item.type==="spell",
    hasSave: item.hasSave && autoCheckSaves === "none",
    hasAreaTarget: false
  };

  const templateType = ["tool"].includes(item.data.type) ? item.data.type : "item";
  const template = `systems/dnd5e/templates/chat/${templateType}-card.html`;
  const html = await renderTemplate(template, templateData);

  //TODO: look at speaker for this message
  // Basic chat message data
  const chatData = {
    user: game.user._id,
    type: CONST.CHAT_MESSAGE_TYPES.OTHER,
    content: html,
    speaker: {
      actor: item.actor._id,
      token: item.actor.token,
      alias: item.actor.name
    }
  };
  // Toggle default roll mode
  let rollMode = game.settings.get("core", "rollMode");
  if ( ["gmroll", "blindroll"].includes(rollMode) ) chatData["whisper"] = ChatMessage.getWhisperRecipients("GM").filter(u=>u.active);
  if ( rollMode === "blindroll" ) chatData["blind"] = true;

  // Create the chat message
  return await ChatMessage.create(chatData);
}

let checkIncapcitated = (actor, item, event) => {
  if(actor.data.data.attributes?.hp?.value <= 0) {
    console.log(`minor-qol | ${actor.name} is incapacitated`)
    ui.notifications.warn(`${actor.name} is incapacitated`)
    return false;
  }
  return true;
}

/** takes two tokens of any size and calculates the distance between them
*** gets the shortest distance betwen two tokens taking into account both tokens size
*** if wallblocking is set then wall are checked
**/
function getDistance (t1, t2, wallblocking = false) {
  //Log("get distance callsed");
  var x, x1, y, y1, d, r, segments=[], rdistance, distance;
  for (x = 0; x < t1.data.width; x++) {
    for (y = 0; y < t1.data.height; y++) {
      const origin = new PIXI.Point(...canvas.grid.getCenter(t1.data.x + (canvas.dimensions.size * x), t1.data.y + (canvas.dimensions.size * y)));
      for (x1 = 0; x1 < t2.data.width; x1++) {
          for (y1 = 0; y1 < t2.data.height; y1++){
          const dest = new PIXI.Point(...canvas.grid.getCenter(t2.data.x + (canvas.dimensions.size * x1), t2.data.y + (canvas.dimensions.size * y1)));
          const r = new Ray(origin, dest)
          if (wallblocking && canvas.walls.checkCollision(r)) {
            //Log(`ray ${r} blocked due to walls`);
            continue;
          }
          segments.push({ray: r});
        }
      }
    }
  }
  // console.log(segments);
  if (segments.length == 0) {
    //Log(`${t2.data.name} full blocked by walls`);
    return -1;
  }
  rdistance = canvas.grid.measureDistances(segments, {gridSpaces:true});
  distance = rdistance[0];
  rdistance.forEach(d=> {if (d < distance) distance = d;});
  return distance;
};

let checkRange = (actor, item, event) => {
  let itemData = item.data.data;
  if ((!itemData.range?.value && itemData.range?.units !== "touch") || !["creature", "ally", "enemy"].includes(itemData.target?.type)) 
    return true;
  let token = getSelfTarget(actor);
  
  if (!token) {
    console.warn(minor-qol | `${game.user.name} no token selected cannot check range`)
    ui.notifications.warn(`${game.user.name} no token selected`)
    return false;
  }

  let range = itemData.range?.value || Math.max(token.w, token.h) / 2 / canvas.scene.data.grid * canvas.scene.data.gridDistance;
  for (let target of game.user.targets) { 
    // check the range
    let distance = getDistance(token, target, autoTarget === "wallsBlock") - 5; // assume 2.5 width for each token

    if (distance > range) {
      console.log(`minor-qol | ${target.name} is too far ${distance} from your character you cannot hit`)
      ui.notifications.warn(`${actor.name}'s target is ${Math.round(distance * 10) / 10} away and your range is only ${range}`)
      return false;
    }
    if (distance < 0) {
      console.log(`minor-qol | ${target.name} is blocked by a wall`)
      ui.notifications.warn(`${actor.name}'s target is blocked by a wall`)
      return false;
    }
  }
  return true;
}

window.MinorQOL = {
  checkSaves: checkSaves,
  resetStatus: resetStatus,
  doCombinedRoll: doCombinedRoll,
  doMacroRoll: doMacroRoll,
  doRoll: doRoll,
  forceRollDamage: false,
  applyTokenDamage: applyTokenDamage,
  _stateData: {}
};
