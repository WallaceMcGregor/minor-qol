# Minor QOL for Foundry VTT requires 0.5.7 and dnd5e 0.91

minor-qol automates some of the steps in handling combat rolls. It supports the standard dnd5e rolls and betterRolls5e with both single and dual rolls. Minor-qol uses targeted tokens for checking hits/damage/saves etc NOT selected targets.

* To use minor-qol with better rolls you should disable "speed rolls" and "add item sheet buttons" in minor-qol.
* All of the other minor-qol flags flags are supposed to work, auto-check hits, saves, apply damage etc.
* Damage immunities/resitance should work, but do check.
* Auto application of dynamiceffects effects is supported (disable via the minor-qol flag if you don't want that).
* When you find bugs (and you will), please try and give as much information as you can to help me work out what is going on.

### Main Features ###
* minor-qol goes through a standard sequence to complete the attack/spell cast. minor-qol assumes that when you click the roll button you mean it. Minor-qol always applies damage/healing to the targeted tokens. If the item specifies a target of "self" minor-qol uses that instead (usually the selected token).
1. Consume any resources that should be consumed (ammunition, charges, spell slot etc).
2. If the item specifies a target template place the template and if enabled select targets inside that template.
2. If the item has an attack roll the attack.
3. If auto check hits is enabled modify the set of targets to be only those that were hit by the attack roll if there was one. If disabled the attack is assumed to hit all targets.
4. Roll the dmage if auto roll damage is enabled. 
5. Roll any saving throws for the affected targets (note there are options for players to roll their own saves). If disabled it is assumed that all targets failed their save.
6. Apply damage if enabled taking into account damage resitance/immunities if enabled. Minor-qol knows that damage can have different types so supports 10 pts of slahsing damage and 15 points of fire damage and applies immunities to each part seperately.
7. If the attack hits, the save fails or damage is applied or there is no other effect for the item (and dynamic effects is enabled) apply the items dynamiceffects active effects to the targeted tokens - please see dynamic effects for more information. If the item has a duration and about-time is enabled schedule the removal of the effect or expiry.

There are various flags that you can set to modify the above sequence.

* minor-qol creates standard chat cards where they exist.  You can choose almost any combination of the following flags to get the behaviour you want.
* Auto Target. When a template is drawn all tokens inside the template are targeted. (mainly useful for area effect spells). Optionally walls between the base/centre of the template block targetting.
* Auto target ranged spells. If the spell specifies a range and targets of creature/enemy/ally they will be auto targeted if in range of the caster. (Enemy always means a hostile creature and ally always means a friendly creature independent of the caster).
* Auto check hits. When an attack roll is made each targeted token is checked to see if it is hit and a chat card displayed. If not enabled it is assumed that all selected targets are hit.
* Auto check saves. When damage is rolled for anything that requies a save all targeted tokens are checked to see if they save and a chat card displayed. When checking saving throws the normal saving throw chat cards are hidden by default. If you set this option to "All see results + rolls" all rolls are display, both npc and PC. If set to none it is assumed no-one saves.
* Magic resitance on saving throws against sepll effects. If the damage resistance trait has a custom value that includes the exact text "magic-resistant" then saving throws agains spells/spell effects will be made with advantage if auto check saves is selected.  In addition if the target has the feat "Magic Resitance" in it's inverntory the roll will be made with advantage.
* Prompt Players to Roll saves. If set to none, all saving throws are auto rolled. If set to Chat Message each active player character (i.e. a logged in player controls the token) a Chat message is whispered to the player roll a saving throw. If set to LMRTFY and the module LMRTFY is active, then a LMRTFY prompt will be displayed on the players client to roll the saving throw. In both cases the players roll will be visible in the chat. IF your players want to roll saves I strongly reccomend using LMRTFY because minor-qol gets more information about the saving throw and can correctly apply it to the cast spell if more than one save is outstanding for the same character.

* Auto roll damage. When an attack roll is made (or a spell that causes damage is cast) roll the damage for the attack/spell. If no tokens are selected damge is rolled anyway. The mdoule will roll critical damage if the roll that triggered it it was a critical. If the roll was a fumble no damage is rolled.

* Auto apply damage. When damage is rolled it is automatically applied to target tokens. However the previous state is remembered. So if auto check hits is enabled only targets that were hit by the attack take damage. If Auto check saves is enabled the saving throw reduces the damage taken (requires damage immunities to be enabled). (minor-qol knows most of the spells that cause no damage on save). Optionally take into account damage immunities when applying damage.

* Use damage immunities if set to none, applies the full damage to targets. Otherwise takes into account each of the damge types in the damage roll, so 2d6 +5 (slashing) + 1d8 (radiant) + 1d4 bless will be treated as (2d6+5+1d4 slashing) + (1d8 radiant) and damage immunities/resitances/vulnerabilites are applied per damage component. This happens for both auto damage and chat damage buttons.  
When "Apply saves - no text check" is selected the above calculations are performed. In addition certain spells are flagged as having no damage on saves, otherwise a save indicates 1/2 damage on any spell attack that specifies a save.
If "Apply saves - check text" is selected the spell description is checked for the text "half as much damage" or "half damage". If found saves take 1/2 damage otherwise full damage is applied. If the text "no damage on save" is found in the description the spell will do no damage on a save.

* Add chat damage buttons. When enabled each chat damage card has apply damage buttons attached. Damage immunities are applied (if enabled) but saves are not so you need to deal with this as GM. Damage buttons apply to the **selected** token (not targeted).

* Speed item rolls. When enabled the item icon on the character sheet will skip the standard roll chat card and roll the attack/cast the spell immediately. When clicking on the icon you can use ctl for a disadvanate roll, alt for a roll with advantage, in addtion right clicking on the icon will do a versatile attack and still support the alt/ctl keys. The rest of the flags determine how much else is rolled. (AutoRollamage is a good choice when this is enabled)

* Auto apply item effects. If dynamiceffects is installed and active minor-qol will auto apply effects under the following conditions:
1. If the effect has an attack/save then the effect will be applid to hit/did not save targets. If you are not auto checking hits/saves it will assume everyone is hit and no one saved.
2. Otherwise if the spell has an action activation (i.e. action/bonus actio/reaction) and has targets speccified (i.e. self/creature) and a target is targeted (not required for self) effects will be applied when the items/spell is used/cast.

- For spells - minor-qol will place the spell template (if there is one), select the targets (if enabled) and then roll the damage. The caster is prompted to select the spell level to cast at. If no spell slot is available the cast will fail unless consume spell slot is deselected.
-  For weapons - The attack roll is made (auto check hits applies) and if auto damage rolls are enabled it auto rolls the damage (auto check damage applies).

## Sample settings.
* Auto roll damage only. When an attack is rolled the damage roll is always made as well. It know about critical hits/fumbles.
* Auto roll damage and auto check hits. Will roll attacks and damage rolls and tell you if attack hit.
* Semi automated. **My preferred setting** Speed item rolls, Auto target, auto roll damage, auto check saves, auto check hits, add damage buttons, damage immunites = saves - no check text. This will place the area effect template (if appropriate), roll the attack, check if the attack hit, roll any saves required and create roll the damage (knowing about criticals/fumbles) and provide damage application buttons and apply damage to the targets.

## Module Incompatibilities ##
**Cozy-plyaer** If you use cozy-player and have remove targets on attach minor-qol will not be able to process any actions that require targets, such as chekcing hits, checking saves or applying damage correctly.
**popout** If you use popout any rolls from the popped out window will not have targets selected.
cozy-player with remove targets on attach breaks all of the automated features in minor-qol.
**Cautious Gamemaster** One option rewrites the message speaker which causes auto damage application to bread.

## Other things
* A confirmation dialog when you delete an item from the character sheet - enable/disable with the "Item Delete Check" flag. 
* You can enable buttons on the inventory sheet. The other buttons just give a short hand for selecting the same functions as from the chat card and support shift/ctl/alt to avoid the dialogs.
* GM sees all whispers. If true (defaults to false) all whispered messages are displayed in the GM's chat log overriding the default behaviour of foundry.
* Colored borders on chat. If enabled draws a border in the users color around each chat message from that user, you can also have the name highlighted in the same color.
* You can choose to display the target token's name or -???- and it's image in the save/hits chat cards. Coupled with CUB this allows anonymous enemies.
* When displaying hit checks or saving throws the GM always sees the token name and clicking on it selects the token - usueful for damage button application.
* When casting a spell the spell is cast a level choice dialog is displayed.
* Auto applied damage creates a chat card for the DM which details the damage applied to the target and an "undo" button so that the DM can reverse the damage applied and do the right thing instead.
* The module also supports speed item rolls on the macro bar. Set the flag "Item macros use speed roll" to true and any new macros created by dragging to the macro bar will be speed item rolls. The speed item macros support shift/ctl/alt. Note if you already have a macro of the same name it will not be overwritten with the new one. Either delete the old and recreate for just edit it using the info below.

* You can create speed item macro rolls by hand, create a script macro and enter (the type and versatile fields are optional - type is useful if you have multiple items with the same name, default behaviour is to just pick the first one it finds that matches the name, versatile if true specifies that you want to make a versatile roll with the weapon, defaults to false) .
```MinorQOL.doMacroRoll(event, "Greataxe", {type: "weapon" verstaile: false})```

* I have added a new setting MinorQOL.forceRollDamage. When set it will ignore the config settings and always roll damage useful for traps.

### DoTrapAttack ###
Lots of people seem to want to implement traps that cause damage to players. To do so requires a macro, the module Trigger Happy installed and the module Furnace installed and with advanced macros turned on.
For it it work the best you require minor-qol with speed item rolls turned on, auto check Hit enabled.
Here is the source code I use for traps in my world. The macro assumes you have some actor setup named (say) TrapEffects and each trap effect you want in the inverntory of the TrapEffects Actor. You can import the macro from

```
// Requires minor-qol to work completely
// Rolls an attack from actor args[0] using item args[1] and display a token args[2]
let tactor = game.actors.entities.find(a => a.name === args[0])
if (!tactor) return `/Whisper GM "DoTrap: Target token ${args[0]} not found"`
let item = tactor.items.find(i=>  i.name === args[1])
if (!item) return `/Whisper GM "DoTrap: Item ${args[1]} not found"`
let oldTargets = game.user.targets;
game.user.targets = new Set();
game.user.targets.add(token);
Hooks.once("MinorQolRollComplete", () => {
    MinorQOL.forceRollDamage=false;
    game.user.targets=oldTargets;
})
MinorQOL.forceRollDamage = true;
await MinorQOL.doCombinedRoll({actor, item, event, token})
let trapToken = canvas.tokens.placeables.find(t=>t.name === args[2])
if (trapToken) await DynamicEffects.setTokenVisibility(trapToken.id, true);

// sample trigger happy line
// @Token[Trap1] @Trigger[capture move] @ChatMessage[/DoTrapAttack TrapEffects "Scything Blade" Trap1]
// requires a token on the map called Trap1
// An actor called TrapEffects (does not need to be on the map)
// The actor TrapEffects needs a weapon called "Scything Blade"
// If using minor-qol the damage will be auto applied
```

### For tranlators ###
For the follwoing fields
```
  "minor-qol.damageRollFlavorTextAlt": " - Damage Roll",
  "minor-qol.attackRollFlavorTextAlt": " - Attack Roll",
  "minor-qol.savingThrowTextAlt": "Saving Throw",
```
Leave these fields (or the non-alt versions) in English so that the rolls will be detected no matter what language the roll flavor text is displayed in.

### Installation Instructions
Install from the foundry vtt install module dialog.

### Bugs

### Feedback
Shoutout to @Red Reign and @Hooking for lots of great code to look at and borrow - you can have it back any time.
Thanks to @errational for being critical of the previous version and encouraging me to rewrite it. I think this is much better.
Special thanks to @BrotherSharp for all his support with translation files.

